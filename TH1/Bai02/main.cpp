#include <iostream>
#include "MangTamGiac.h"
using namespace std;

int main() {
    MangTamGiac a;
    a.nhap();

    cout << "Tam giac deu:" << endl;
    a.liet_ke_deu();

    cout << "Tam giac can:" << endl;
    a.liet_ke_can();

    cout << "Tam giac vuong:" << endl;
    a.liet_ke_vuong();

    cout << "Tam giac vuong can:" << endl;
    a.liet_ke_vuong_can();

    cout << "Tam giac co chu vi nho nhat:" << endl;
    a.tim_chu_vi_lon_nhat();

    cout << "Tam giac co chu vi lon nhat:" << endl;
    a.tim_chu_vi_lon_nhat();

    cout << "Tam giac co dien tich nho nhat:" << endl;
    a.tim_dien_tich_nho_nhat();

    cout << "Tam giac co dien tich lon nhat:" << endl;
    a.tim_dien_tich_lon_nhat();

    cout << "Sap chu vi tang" << endl;
    a.sap_chu_vi_tang();
    a.in();

    cout << "Sap dien tich giam" << endl;
    a.sap_dien_tich_giam();
    a.in();

    return 0;
}
