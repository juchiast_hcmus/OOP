#include <iostream>
#include <cmath>
#include "TamGiac.h"
using namespace std;

void TamGiac::tinh_canh(double &d1, double &d2, double &d3) {
    d1 = a.khoang_cach(b);
    d2 = b.khoang_cach(c);
    d3 = c.khoang_cach(a);
}

void TamGiac::nhap() {
    a.nhap();
    b.nhap();
    c.nhap();
}

void TamGiac::in() {
    a.in();
    cout << ',';
    b.in();
    cout << ',';
    c.in();
}

double TamGiac::chu_vi() {
    return a.khoang_cach(b) + b.khoang_cach(c) + c.khoang_cach(a);
}

double TamGiac::dien_tich() {
    double d1, d2, d3;
    tinh_canh(d1, d2, d3);
    double p = (d1 + d2 + d3) / 2.0;
    return sqrt(p*(p-d1)*(p-d2)*(p-d3));
}

bool TamGiac::khong_phai_tam_giac() {
    double d1, d2, d3;
    tinh_canh(d1, d2, d3);
    return !(d1+d2>d3 && d1+d3>d2 && d2+d3>d1);
}

bool TamGiac::deu() {
    double d1, d2, d3;
    tinh_canh(d1, d2, d3);
    return d1==d2 && d2==d3;
}

bool TamGiac::can() {
    double d1, d2, d3;
    tinh_canh(d1, d2, d3);
    return d1==d2 || d2==d3 || d3==d1;
}

bool TamGiac::vuong() {
    double d1, d2, d3;
    tinh_canh(d1, d2, d3);
    if (d1 < d2) swap(d1, d2);
    if (d1 < d3) swap(d1, d3);
    return d1*d1 == d2*d2 + d3*d3;
}

Loai TamGiac::xet_loai() {
    if (khong_phai_tam_giac()) return Loai::KhongPhaiTamGiac;

    if (deu()) return Loai::Deu;

    bool vuong = this->vuong();
    bool can = this->can();

    if (vuong && can) return Loai::VuongCan;

    if (vuong) return Loai::Vuong;
    if (can) return Loai::Can;

    return Loai::Thuong;
}

