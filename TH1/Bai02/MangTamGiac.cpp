#include <iostream>
#include <cmath>
#include "MangTamGiac.h"
using namespace std;


void MangTamGiac::in() {
    for (int i=0; i<num; i++) {
        data[i].in();
        cout << endl;
    }
}

void MangTamGiac::nhap() {
    cout << "Nhap so tam giac: ";
    cin >> num;
    data = new TamGiac[num];
    for (int i=0; i<num; i++) {
        cout << "Nhap tam giac " << i << ": ";
        data[i].nhap();
    }
}

void MangTamGiac::liet_ke(Loai l) {
    for (int i=0; i<num; i++) if (data[i].xet_loai() == l) {
        data[i].in();
        cout << endl;
    }
}

void MangTamGiac::liet_ke_can() { liet_ke(Loai::Can); }
void MangTamGiac::liet_ke_vuong() { liet_ke(Loai::Vuong); }
void MangTamGiac::liet_ke_deu() { liet_ke(Loai::Deu); }
void MangTamGiac::liet_ke_vuong_can() { liet_ke(Loai::VuongCan); }

void MangTamGiac::tim_chu_vi_lon_nhat() {
    int r = 0;
    for (int i=1; i<num; i++) {
        if (data[r].chu_vi() < data[i].chu_vi()) {
            r = i;
        }
    }
    data[r].in();
    cout << endl;
}
void MangTamGiac::tim_chu_vi_nho_nhat() {
    int r = 0;
    for (int i=1; i<num; i++) {
        if (data[r].chu_vi() > data[i].chu_vi()) {
            r = i;
        }
    }
    data[r].in();
    cout << endl;
}
void MangTamGiac::tim_dien_tich_lon_nhat() {
    int r = 0;
    for (int i=1; i<num; i++) {
        if (data[r].dien_tich() < data[i].dien_tich()) {
            r = i;
        }
    }
    data[r].in();
    cout << endl;
}
void MangTamGiac::tim_dien_tich_nho_nhat() {
    int r = 0;
    for (int i=1; i<num; i++) {
        if (data[r].dien_tich() > data[i].dien_tich()) {
            r = i;
        }
    }
    data[r].in();
    cout << endl;
}
void MangTamGiac::sap_chu_vi_tang() {
    for (int i=0; i<num; i++) {
        for (int j=i+1; j<num; j++) {
            if (data[i].chu_vi() > data[j].chu_vi()) {
                swap(data[i], data[j]);
            }
        }
    }
}
void MangTamGiac::sap_dien_tich_giam() {
    for (int i=0; i<num; i++) {
        for (int j=i+1; j<num; j++) {
            if (data[i].dien_tich() < data[j].dien_tich()) {
                swap(data[i], data[j]);
            }
        }
    }
}
