#ifndef TAMGIAC_H
#define TAMGIAC_H
#include "Diem.h"
enum class Loai {
    KhongPhaiTamGiac,
    Deu,
    Can,
    Vuong,
    VuongCan,
    Thuong,
};

class TamGiac {
    Diem a, b, c;
    bool khong_phai_tam_giac();
    bool deu();
    bool can();
    bool vuong();
    void tinh_canh(double &, double &, double &);
    public:
    void nhap();
    void in();
    double chu_vi();
    double dien_tich();
    Loai xet_loai();
};
#endif
