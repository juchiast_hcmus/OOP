#ifndef MANGTAMGIAC_H
#define MANGTAMGIAC_H
#include "TamGiac.h"
class MangTamGiac {
    int num;
    TamGiac *data;
    void liet_ke(Loai);
    public:
    void in();
    void nhap();
    void liet_ke_deu();
    void liet_ke_can();
    void liet_ke_vuong();
    void liet_ke_vuong_can();
    void tim_chu_vi_nho_nhat();
    void tim_chu_vi_lon_nhat();
    void tim_dien_tich_nho_nhat();
    void tim_dien_tich_lon_nhat();
    void sap_chu_vi_tang();
    void sap_dien_tich_giam();
};
#endif
