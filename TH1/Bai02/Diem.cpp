#include <iostream>
#include <cmath>
#include "Diem.h"
using namespace std;

void Diem::nhap() {
    cin >> x >> y;
}

void Diem::in() {
    cout << '(' << x << ',' << y << ')';
}

double sqr(double x) {
    return x * x;
}

double Diem::khoang_cach(const Diem &d) {
    return sqrt(sqr(x - d.x) + sqr(y - d.y));
}
