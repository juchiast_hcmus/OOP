#include <iostream>
#include "MangDuongTron.h"
using namespace std;

void MangDuongTron::in() {
    for (int i=0; i<num; i++) {
        data[i].in();
        cout << endl;
    }
}

void MangDuongTron::nhap() {
    cout << "Nhap so duong tron: ";
    cin >> num;
    data = new DuongTron[num];
    for (int i=0; i<num; i++) {
        cout << "Nhap duong tron " << i << ": ";
        data[i].nhap();
    }
}

void MangDuongTron::tim_chu_vi_lon_nhat() {
    int r = 0;
    for (int i=1; i<num; i++) {
        if (data[r].chu_vi() < data[i].chu_vi()) {
            r = i;
        }
    }
    data[r].in();
    cout << endl;
}
void MangDuongTron::tim_chu_vi_nho_nhat() {
    int r = 0;
    for (int i=1; i<num; i++) {
        if (data[r].chu_vi() > data[i].chu_vi()) {
            r = i;
        }
    }
    data[r].in();
    cout << endl;
}
void MangDuongTron::tim_dien_tich_lon_nhat() {
    int r = 0;
    for (int i=1; i<num; i++) {
        if (data[r].dien_tich() < data[i].dien_tich()) {
            r = i;
        }
    }
    data[r].in();
    cout << endl;
}
void MangDuongTron::tim_dien_tich_nho_nhat() {
    int r = 0;
    for (int i=1; i<num; i++) {
        if (data[r].dien_tich() > data[i].dien_tich()) {
            r = i;
        }
    }
    data[r].in();
    cout << endl;
}
void MangDuongTron::sap_chu_vi_tang() {
    for (int i=0; i<num; i++) {
        for (int j=i+1; j<num; j++) {
            if (data[i].chu_vi() > data[j].chu_vi()) {
                swap(data[i], data[j]);
            }
        }
    }
}
void MangDuongTron::sap_dien_tich_giam() {
    for (int i=0; i<num; i++) {
        for (int j=i+1; j<num; j++) {
            if (data[i].dien_tich() < data[j].dien_tich()) {
                swap(data[i], data[j]);
            }
        }
    }
}
