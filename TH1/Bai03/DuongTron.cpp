#include <iostream>
#include "DuongTron.h"
using namespace std;


void DuongTron::nhap() {
    cin >> x >> y >> r;
}

void DuongTron::in() {
    cout << "(" << x << "," << y << ") " << r;
}

const double PI = 3.14159265358979323846;

double DuongTron::chu_vi() {
    return 2.0*PI*r;
}

double DuongTron::dien_tich() {
    return PI*r*r;
}


