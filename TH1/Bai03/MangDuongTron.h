#ifndef MANGDUONGTRON_H
#define MANGDUONGTRON_H
#include "DuongTron.h"
class MangDuongTron {
    int num;
    DuongTron *data;
    public:
    void in();
    void nhap();
    void tim_chu_vi_nho_nhat();
    void tim_chu_vi_lon_nhat();
    void tim_dien_tich_nho_nhat();
    void tim_dien_tich_lon_nhat();
    void sap_chu_vi_tang();
    void sap_dien_tich_giam();
};
#endif
