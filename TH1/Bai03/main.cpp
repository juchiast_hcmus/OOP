#include <iostream>
#include "MangDuongTron.h"
using namespace std;

int main() {
    MangDuongTron a;
    a.nhap();

    cout << "Duong tron co chu vi nho nhat:" << endl;
    a.tim_chu_vi_lon_nhat();

    cout << "Duong tron co chu vi lon nhat:" << endl;
    a.tim_chu_vi_lon_nhat();

    cout << "Duong tron co dien tich nho nhat:" << endl;
    a.tim_dien_tich_nho_nhat();

    cout << "Duong tron co dien tich lon nhat:" << endl;
    a.tim_dien_tich_lon_nhat();

    cout << "Sap chu vi tang" << endl;
    a.sap_chu_vi_tang();
    a.in();

    cout << "Sap dien tich giam" << endl;
    a.sap_dien_tich_giam();
    a.in();

    return 0;
}
