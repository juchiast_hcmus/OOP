#include <iostream>
#include "don_thuc.h"

DonThuc DonThuc::operator + (const DonThuc &d) {
    if (n != d.n) throw 0;
    return DonThuc(a + d.a, n);
}
DonThuc DonThuc::operator * (const DonThuc &d) {
    return DonThuc(a * d.a, n + d.n);
}

bool DonThuc::operator < (const DonThuc &d) {
    if (n < d.n) return true;
    if (n > d.n) return false;
    return a < d.a;
}
bool DonThuc::operator > (const DonThuc &d) {
    if (n > d.n) return true;
    if (n < d.n) return false;
    return a > d.a;

}
bool DonThuc::operator <= (const DonThuc &d) {
    return !(*this > d);
}
bool DonThuc::operator >= (const DonThuc &d) {
    return !(*this < d);
}
bool DonThuc::operator == (const DonThuc &d) {
    return a == d.a && n == d.n;
}
bool DonThuc::operator != (const DonThuc &d) {
    return !(*this == d);
}

DonThuc& DonThuc::operator = (const DonThuc &d) {
    a = d.a;
    n = d.n;
    return *this;
}
DonThuc& DonThuc::operator += (const DonThuc &d) {
    if (n != d.n) throw 0;
    a += d.a;
    return *this;
}
DonThuc& DonThuc::operator *= (const DonThuc &d) {
    n += d.n;
    return *this;
}

DonThuc& DonThuc::operator ++() {
    n++;
    return *this;
}
DonThuc& DonThuc::operator --() {
    n--;
    return *this;
}
DonThuc DonThuc::operator ++ (int) {
    DonThuc tmp = *this;
    ++(*this);
    return tmp;
}
DonThuc DonThuc::operator -- (int) {
    DonThuc tmp = *this;
    --(*this);
    return tmp;
}

DonThuc DonThuc::operator !() {
    return DonThuc(n*a, n-1);
}

std::ostream& operator << (std::ostream &os, const DonThuc &d) {
    return os << d.a << 'x' << '^' << d.n;
}
std::istream& operator >> (std::istream &is, DonThuc &d) {
    return is >> d.a >> d.n;
}
