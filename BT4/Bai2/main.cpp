#include "don_thuc.h"
using namespace std;

int main() {
    DonThuc d;
    cout << "Nhap don thuc: ";
    cin >> d;
    cout << d << endl;
    cout << !d << endl;
    cout << ++d << endl;
    cout << d++ << endl;
    cout << d-- << endl;
    cout << --d << endl;
    return 0;
}
