#ifndef DON_THUC_H
#define DON_THUC_H
#include <iostream>

class DonThuc {
    int a, n;
    public:
    DonThuc(): a(0), n(0) {}
    DonThuc(int a, int n): a(a), n(n) {}
    
    DonThuc operator + (const DonThuc &);
    DonThuc operator * (const DonThuc &);
    
    bool operator < (const DonThuc &);
    bool operator > (const DonThuc &);
    bool operator <= (const DonThuc &);
    bool operator >= (const DonThuc &);
    bool operator == (const DonThuc &);
    bool operator != (const DonThuc &);

    DonThuc& operator = (const DonThuc &);
    DonThuc& operator += (const DonThuc &);
    DonThuc& operator *= (const DonThuc &);

    DonThuc& operator ++();
    DonThuc& operator --();
    DonThuc operator ++ (int);
    DonThuc operator -- (int);

    DonThuc operator !();

    friend std::ostream& operator << (std::ostream &, const DonThuc &);
    friend std::istream& operator >> (std::istream &, DonThuc &);
};
#endif
