#ifndef ARRAY_H
#define ARRAY_H

#include <cstdint>
#include <iostream>
#include <cstring>
using std::size_t;

template<class T>
class Array {
    size_t num;
    T *data;
    public:
    Array();
    Array(size_t);
    Array(size_t, T*);
    Array(const Array<T> &);
    ~Array();

    Array<T>& operator = (const Array<T> &);
    T& operator [] (size_t i);
    operator T*();

    template<class U>
    friend std::ostream& operator << (std::ostream &, Array<U> &);
    template<class U>
    friend std::istream& operator >> (std::istream &, Array<U> &);
};

template<class T>
std::ostream& operator << (std::ostream &os, Array<T> &array) {
    for (size_t i=0; i<array.num; i++) {
        os << array[i] << ", ";
    }
    return os;
}
template<class T>
std::istream& operator >> (std::istream &is, Array<T> &array) {
    size_t num;
    is >> num;
    Array<T> tmp(num);
    for (size_t i=0; i<num; i++) {
        is >> tmp[i];
    }
    array = tmp;
}

template<class T>
Array<T>::Array(): num(0), data(nullptr) {}

template<class T>
Array<T>::Array(size_t size): num(size), data(new T[size]) {
    std::memset(data, 0, sizeof(T)*num);
}

template<class T>
Array<T>::Array(size_t size, T *ptr): num(size), data(new T[size]) {
    for (size_t i=0; i<num; i++) data[i] = ptr[i];
}

template<class T>
Array<T>::Array(const Array<T> &arr): num(arr.num), data(new T[num]) {
    for (size_t i=0; i<num; i++) data[i] = arr.data[i];
}

template<class T>
Array<T>::~Array() {
    delete[] data;
}

template<class T>
Array<T>& Array<T>::operator = (const Array<T> &arr) {
    num = arr.num;
    if (data != nullptr) {
        delete[] data;
    }
    data = new T[num];
    for (size_t i=0; i<num; i++) data[i] = arr.data[i];
}
template<class T>
T& Array<T>::operator [] (size_t i) {
    if (i >= num) throw 0;
    return data[i];
}
template<class T>
Array<T>::operator T*() {
    return data;
}

#endif
