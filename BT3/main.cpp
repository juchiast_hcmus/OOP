#include "array.h"

int main() {
    Array<int> a;
    a.print();
    float f[] = {1.3, 2.4, 0.1231, -0.2, -32.31};
    Array<float> b(5, f);
    b.print();
    return 0;
}
