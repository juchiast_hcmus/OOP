#include <cstdint>
#include <iostream>
#include <cstring>

template<class T>
class Array {
    std::size_t num;
    T *data;
    public:
    Array();
    Array(std::size_t);
    Array(std::size_t, T*);
    Array(const Array<T> &);
    ~Array();
    void print();
};

template<class T>
Array<T>::Array(): num(0), data(nullptr) {}

template<class T>
Array<T>::Array(std::size_t size): num(size), data(new T[size]) {
    std::memset(data, 0, sizeof(T)*num);
}

template<class T>
Array<T>::Array(std::size_t size, T *ptr): num(size), data(new T[size]) {
    std::memcpy(data, ptr, sizeof(T)*num);
}

template<class T>
Array<T>::Array(const Array<T> &arr) {
    num = arr.num;
    data = new T[num];
    std::memcpy(data, arr.data, sizeof(T)*num);
}

template<class T>
Array<T>::~Array() {
    delete[] data;
}

template<class T>
void Array<T>::print() {
    for (int i=0; i<num; i++) {
        std::cout << data[i] << ", ";
    }
    std::cout << std::endl;
}
