#pragma once
#include <string>
using namespace std;

class CItem {
protected:
	bool read_only, hidden;
	string name;
public:
	CItem(const string &name) : read_only(false), hidden(false), name(name) {}
	const string& getName() const { return name; }
	bool isHidden() const { return hidden; }
	virtual void setHidden(bool, bool) = 0;
	virtual void print(bool) = 0;
};