#pragma once
#include "CItem.h"
#include <iostream>
#include <ctime>

class CLogger {
	ostream &output;
	CItem *ptr;
	static string getDate() {
		std::time_t t;
		time(&t);
		std::tm *tm = localtime(&t);
		char buffer[100];
		strftime(buffer, 100, "%d/%m/%Y %I:%M:%S", tm);
		return string(buffer);
	}
public:
	CLogger(CItem *ptr, ostream &o = cout) : ptr(ptr), output(o) {}
	CLogger& operator = (CItem *p) {
		ptr = p;
		return *this;
	}
	bool operator != (void *p) const { return ptr != p; }

	const string& getName() const {
		output << getDate() << ' ' << "getName()" << endl;
		return ptr->getName();
	}
	bool isHidden() const {
		output << getDate() << ' ' << "isHidden()" << endl;
		return ptr->isHidden();
	}
	void setHidden(bool a, bool b) {
		output << getDate() << ' ' << "setHidden(" << a << ',' << b << ')' << endl;
		return ptr->setHidden(a, b);
	}
	void print(bool a) {
		output << getDate() << ' ' << "print(" << a << ')' << endl;
		return ptr->print(a);
	}
};