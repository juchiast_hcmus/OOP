#pragma once
#include "CItem.h"

class CFile : public CItem {
	int size;
public:
	CFile(const string &name, int size) : CItem(name), size(size) {}
	void setHidden(bool b, bool) {
		hidden = b;
	}
	void print(bool) {}
};