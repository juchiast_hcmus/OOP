#pragma once
#include "CItem.h"
#include <vector>
#include <iostream>
using namespace std;

class CFolder : public CItem {
	vector<CItem *> items;
public:
	CFolder(const string &name) : CItem(name) {}
	void add(CItem *item) {
		if (item) items.push_back(item);
	}
	CItem *findByName(const string &name) {
		for (CItem *x : items) if (x->getName() == name) return x;
		for (CItem *x : items) if (dynamic_cast<CFolder*>(x)) {
			CItem *p = dynamic_cast<CFolder*>(x)->findByName(name);
			if (p) return p;
		}
		return NULL;
	}
	CItem *removeByName(const string &name) {
		for (int i = 0; i < items.size(); i++) {
			if (items[i]->getName() == name) {
				CItem *x = items[i];
				items[i] = items.back();
				items.pop_back();
				return x;
			}
		}
		for (CItem *x : items) if (dynamic_cast<CFolder*>(x)) {
			CItem *p = dynamic_cast<CFolder*>(x)->findByName(name);
			if (p) return p;
		}
		return NULL;
	}
	void print(bool r) {
		for (CItem *x : items) if (!x->isHidden()) cout << x->getName() << endl;
		if (r) {
			for (CItem *x : items) if (!x->isHidden()) if (dynamic_cast<CFolder *>(x)) {
				dynamic_cast<CFolder *>(x)->print(r);
			}
		}
	}
	void setHidden(bool b, bool r) {
		hidden = b;
		if (r) {
			for (CItem *x : items) x->setHidden(b, r);
		}
	}
};
