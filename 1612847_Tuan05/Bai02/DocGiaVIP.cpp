#include "DocGiaVIP.h"
#include <iostream>
using namespace std;

#define CS const std::string &

DocGiaVIP::DocGiaVIP() : DocGia() {}
DocGiaVIP::DocGiaVIP(CS a) : DocGia(a) {}
DocGiaVIP::DocGiaVIP(CS a, CS b) : DocGia(a, b) {}
DocGiaVIP::DocGiaVIP(CS a, CS b, Date c) : DocGia(a, b, c) {}
DocGiaVIP::DocGiaVIP(CS a, CS b, Date c, bool d) : DocGia(a, b, c, d) {}

void DocGiaVIP::Nhap() {
	DocGia::Nhap();
}
void DocGiaVIP::Xuat() const {
	DocGia::Xuat();
	cout << "Phi: " << tinhPhi() << endl;
}
int DocGiaVIP::tinhPhi() const {
	return 50000;
}