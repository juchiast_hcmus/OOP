#pragma once
#include "DocGia.h"
class DocGiaVIP :
	public DocGia
{
public:
	DocGiaVIP();
	DocGiaVIP(const std::string &);
	DocGiaVIP(const std::string &, const std::string &);
	DocGiaVIP(const std::string &, const std::string &, Date);
	DocGiaVIP(const std::string &, const std::string &, Date, bool);
	void Nhap();
	void Xuat() const;
	int tinhPhi() const;
};