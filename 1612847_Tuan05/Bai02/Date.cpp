#include "Date.h"
#include <iostream>
using namespace std;

Date::Date() : m_ngay(1), m_thang(1), m_nam(1) {}

Date::Date(int ngay, int thang, int nam) : m_ngay(ngay), m_thang(thang), m_nam(nam) {}

void Date::Nhap() {
	cin >> m_ngay >> m_thang >> m_nam;
	cin.ignore();
}

std::ostream& operator << (std::ostream& os, const Date &d) {
	return os << d.m_ngay << '/' << d.m_thang << '/' << d.m_nam;
}