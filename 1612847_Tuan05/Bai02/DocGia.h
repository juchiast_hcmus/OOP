#pragma once
#include <string>
#include "Date.h"
class DocGia
{
	std::string m_ma, m_ten;
	Date m_ngayHetHan;
	bool m_gioiTinh;
public:
	DocGia();
	DocGia(const std::string &);
	DocGia(const std::string &, const std::string &);
	DocGia(const std::string &, const std::string &, Date);
	DocGia(const std::string &, const std::string &, Date, bool);
	void Nhap();
	void Xuat() const;
	int tinhPhi() const;
};