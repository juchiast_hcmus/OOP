#include "DocGia.h"
#include <iostream>
using namespace std;

#define CS const std::string &

DocGia::DocGia() {}
DocGia::DocGia(CS ma) : m_ma(ma) {}
DocGia::DocGia(CS ma, CS ten) : m_ma(ma), m_ten(ten) {}
DocGia::DocGia(CS ma, CS ten, Date n) : m_ma(ma), m_ten(ten), m_ngayHetHan(n) {}
DocGia::DocGia(CS ma, CS ten, Date n, bool gt) : m_ma(ma), m_ten(ten), m_ngayHetHan(n), m_gioiTinh(gt) {}

void DocGia::Nhap() {
	cout << "Nhap ma doc gia: "; getline(cin, m_ma);
	cout << "Nhap ten: "; getline(cin, m_ten);
	cout << "Nhap ngay het han: "; m_ngayHetHan.Nhap();
	string tmp;
	cout << "Nhap gioi tinh (nam/nu): "; getline(cin, tmp);
	m_gioiTinh = tmp == "nam";
}

void DocGia::Xuat() const {
	cout << "Ma: " << m_ma << endl;
	cout << "Ten: " << m_ten << endl;
	cout << "Ngay het han: " << m_ngayHetHan << endl;
	cout << "Gioi tinh: " << (m_gioiTinh ? "Nam" : "Nu") << endl;
}

int DocGia::tinhPhi() const { return 0; }