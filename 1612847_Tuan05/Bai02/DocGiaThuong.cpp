#include "DocGiaThuong.h"
#include <iostream>
using namespace std;

#define CS const std::string &

DocGiaThuong::DocGiaThuong() : DocGia() {}
DocGiaThuong::DocGiaThuong(int ss, CS a) : DocGia(a), m_soSach(ss) {}
DocGiaThuong::DocGiaThuong(int ss, CS a, CS b) : DocGia(a, b), m_soSach(ss) {}
DocGiaThuong::DocGiaThuong(int ss, CS a, CS b, Date c) : DocGia(a, b, c), m_soSach(ss) {}
DocGiaThuong::DocGiaThuong(int ss, CS a, CS b, Date c, bool d) : DocGia(a, b, c, d), m_soSach(ss) {}

void DocGiaThuong::Nhap() {
	DocGia::Nhap();
	cout << "Nhap so sach muon: "; cin >> m_soSach; cin.ignore();
}
void DocGiaThuong::Xuat() const {
	DocGia::Xuat();
	cout << "So sach muon: " << m_soSach << endl;
	cout << "Phi: " << tinhPhi() << endl;
}
int DocGiaThuong::tinhPhi() const {
	return 5000 * m_soSach;
}