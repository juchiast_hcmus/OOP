#pragma once
#include <iostream>
class Date
{
	int m_ngay, m_thang, m_nam;
public:
	Date();
	Date(int, int, int);
	void Nhap();
	friend std::ostream& operator << (std::ostream &, const Date &);
};

