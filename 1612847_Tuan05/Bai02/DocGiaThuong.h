#pragma once
#include "DocGia.h"
class DocGiaThuong :
	public DocGia
{
	int m_soSach;
public:
	DocGiaThuong();
	DocGiaThuong(int, const std::string &);
	DocGiaThuong(int, const std::string &, const std::string &);
	DocGiaThuong(int, const std::string &, const std::string &, Date);
	DocGiaThuong(int, const std::string &, const std::string &, Date, bool);
	void Nhap();
	void Xuat() const;
	int tinhPhi() const;
};