#pragma once
#include <string>
#include "Ngay.h"
using std::string;
class NhanVien
{
	string ma_nhan_vien;
	string ho_ten;
	string dia_chi;
	Ngay ngay_sinh;
public:
	NhanVien();
	NhanVien(const string &);
	NhanVien(const string &, const string &);
	NhanVien(const string &, const string &, const string &);
	NhanVien(const string &, const string &, const string &, Ngay);
	void xuat_thong_tin() const;
	void nhap_thong_tin();
};