#include "Ngay.h"

Ngay::Ngay(int ngay, int thang, int nam) : ngay(ngay), thang(thang), nam(nam)
{
}

Ngay::Ngay() : ngay(1), thang(1), nam(1)
{
}

std::ostream& operator << (std::ostream &os, const Ngay &d) {
	return os << d.ngay << '/' << d.thang << '/' << d.nam;
}
std::istream& operator >> (std::istream &is, Ngay &d) {
	return is >> d.ngay >> d.thang >> d.nam;
}