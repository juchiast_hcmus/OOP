#pragma once
#include "NhanVien.h"
#include "Ngay.h"
#include <string>
using std::string;
class NVCongNhat :
	public NhanVien
{
	int so_ngay_lam;
public:
	NVCongNhat();
	NVCongNhat(const string &, int);
	NVCongNhat(const string &, const string &, int);
	NVCongNhat(const string &, const string &, const string &, int);
	NVCongNhat(const string &, const string &, const string &, Ngay, int);
	int tinh_luong() const;
	void nhap_thong_tin();
	void xuat_thong_tin() const;
};
