#include "NhanVien.h"

#include <iostream>
NhanVien::NhanVien() {}
NhanVien::NhanVien(const string &ma) : ma_nhan_vien(ma) {}
NhanVien::NhanVien(const string &ma, const string &ten) : ma_nhan_vien(ma), ho_ten(ten) {}
NhanVien::NhanVien(const string &ma, const string &ten, const string &diachi) : ma_nhan_vien(ma), ho_ten(ten), dia_chi(diachi) {}
NhanVien::NhanVien(const string &ma, const string &ten, const string &diachi, Ngay ngay) : ma_nhan_vien(ma), ho_ten(ten), dia_chi(diachi), ngay_sinh(ngay) {}

void NhanVien::xuat_thong_tin() const {
	using std::cout;
	using std::endl;
	cout << endl;
	cout << "Ma nhan vien: " << ma_nhan_vien << endl;
	cout << "Ho ten: " << ho_ten << endl;
	cout << "Dia chi: " << dia_chi << endl;
	cout << "Ngay sinh: " << ngay_sinh << endl;
}

void NhanVien::nhap_thong_tin() {
	using std::cin;
	using std::cout;
	using std::endl;
	cout << "Nhap thong tin nhan vien: " << endl;
	cout << "Ma nhan vien: ";
	getline(cin, ma_nhan_vien);
	cout << "Ho ten: ";
	getline(cin, ho_ten);
	cout << "Dia chi: ";
	getline(cin, dia_chi);
	cout << "Ngay sinh: ";
	cin >> ngay_sinh;
	cin.ignore();
}