#include "NVCongNhat.h"
#include <iostream>

NVCongNhat::NVCongNhat() : NhanVien(), so_ngay_lam(0) {}
NVCongNhat::NVCongNhat(const string &a, int s) : NhanVien(a), so_ngay_lam(s) {}
NVCongNhat::NVCongNhat(const string &a, const string &b, int s) : NhanVien(a, b), so_ngay_lam(s) {}
NVCongNhat::NVCongNhat(const string &a, const string &b, const string &c, int s) : NhanVien(a, b, c), so_ngay_lam(s) {}
NVCongNhat::NVCongNhat(const string &a, const string &b, const string &c, Ngay d, int s) : NhanVien(a, b, c, d), so_ngay_lam(s) {}
int NVCongNhat::tinh_luong() const {
	return 300000 * so_ngay_lam;
}

void NVCongNhat::nhap_thong_tin() {
	using std::cin;
	using std::cout;
	using std::endl;
	NhanVien::nhap_thong_tin();
	cout << "So ngay lam: ";
	cin >> so_ngay_lam;
	cin.ignore();
}

void NVCongNhat::xuat_thong_tin() const {
	using std::cout;
	using std::endl;
	NhanVien::xuat_thong_tin();
	cout << "So ngay lam: " << so_ngay_lam << endl;
	cout << "Luong: " << tinh_luong() << endl;
}