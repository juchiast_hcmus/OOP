#pragma once
#include <iostream>
class Ngay
{
	int ngay, thang, nam;
public:
	Ngay();
	Ngay(int, int, int);
	friend std::ostream& operator << (std::ostream &, const Ngay &);
	friend std::istream& operator >> (std::istream &, Ngay &);
};

