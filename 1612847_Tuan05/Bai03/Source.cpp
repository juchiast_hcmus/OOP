// Do Hoang Anh Duy
// 1612847
// Bai 03

#include "GiaoVien.h"
#include "GVCN.h"
#include <iostream>
using namespace std;

int main() {
	GiaoVien a;
	a.Nhap();
	a.Xuat();
	cout << "Luong: " << a.tinhLuong() << endl;
	GVCN b;
	b.Nhap();
	b.Xuat();
	cout << "Luong: " << b.tinhLuong() << endl;
	return 0;
}