#include "GiaoVien.h"
#include <iostream>
using namespace std;

GiaoVien::GiaoVien() : m_heSo(0), m_luongCoBan(0), m_soNgayNghi(0) {}
GiaoVien::GiaoVien(const string &ma) : m_ma(ma), m_heSo(0), m_luongCoBan(0), m_soNgayNghi(0) {}
GiaoVien::GiaoVien(const string &ma, const string &ten) : m_ma(ma), m_ten(ten), m_heSo(0), m_luongCoBan(0), m_soNgayNghi(0) {}
GiaoVien::GiaoVien(const string &ma, const string &ten, int hs, int cb) : m_ma(ma), m_ten(ten), m_heSo(hs), m_luongCoBan(cb), m_soNgayNghi(0) {}
GiaoVien::GiaoVien(const string &ma, const string &ten, int hs, int cb, int nn) : m_ma(ma), m_ten(ten), m_heSo(hs), m_luongCoBan(cb), m_soNgayNghi(nn) {}
void GiaoVien::Nhap() {
	cout << "Nhap ma: "; getline(cin, m_ma);
	cout << "Nhap ten: "; getline(cin, m_ten);
	cout << "Nhap he so luong: "; cin >> m_heSo; cin.ignore();
	cout << "Nhap luong co ban: "; cin >> m_luongCoBan; cin.ignore();
	cout << "Nhap so ngay nghi: "; cin >> m_soNgayNghi; cin.ignore();
}
void GiaoVien::Xuat() const {
	cout << "Ma: " << m_ma << endl;
	cout << "Ten: " << m_ten << endl;
	cout << "He so luong: " << m_heSo << endl;
	cout << "Luong co ban: " << m_luongCoBan << endl;
	cout << "So ngay nghi: " << m_soNgayNghi << endl;
}
int GiaoVien::tinhLuong() const {
	return m_heSo * m_luongCoBan - m_soNgayNghi * 100000;
}