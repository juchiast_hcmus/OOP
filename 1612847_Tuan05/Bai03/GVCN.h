#pragma once
#include "GiaoVien.h"
class GVCN :
	public GiaoVien
{
	std::string m_lopChuNhiem;
public:
	GVCN();
	GVCN(const std::string &, const std::string &);
	GVCN(const std::string &, const std::string &, const std::string &);
	GVCN(const std::string &, const std::string &, const std::string &, int, int);
	GVCN(const std::string &, const std::string &, const std::string &, int, int, int);
	void Nhap();
	void Xuat() const;
	int tinhLuong() const;
};