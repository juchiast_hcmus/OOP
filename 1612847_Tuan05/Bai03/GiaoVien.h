#pragma once
#include <string>
class GiaoVien
{
	std::string m_ma, m_ten;
	int m_heSo, m_luongCoBan, m_soNgayNghi;
public:
	GiaoVien();
	GiaoVien(const std::string &);
	GiaoVien(const std::string &, const std::string &);
	GiaoVien(const std::string &, const std::string &, int, int);
	GiaoVien(const std::string &, const std::string &, int, int, int);
	void Nhap();
	void Xuat() const;
	int tinhLuong() const;
};