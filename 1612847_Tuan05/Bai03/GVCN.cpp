#include "GVCN.h"

#include <iostream>
using namespace std;
#define CS const std::string &

GVCN::GVCN() : GiaoVien() {}
GVCN::GVCN(CS lop, CS a) : GiaoVien(a), m_lopChuNhiem(lop) {}
GVCN::GVCN(CS lop, CS a, CS b) : GiaoVien(a, b), m_lopChuNhiem(lop) {}
GVCN::GVCN(CS lop, CS a, CS b, int c, int d) : GiaoVien(a, b, c, d), m_lopChuNhiem(lop) {}
GVCN::GVCN(CS lop, CS a, CS b, int c, int d, int e) : GiaoVien(a, b, c, d, e), m_lopChuNhiem(lop) {}

void GVCN::Nhap() {
	GiaoVien::Nhap();
	cout << "Nhap lop chu nhiem: "; getline(cin, m_lopChuNhiem);
}
void GVCN::Xuat() const {
	GiaoVien::Xuat();
	cout << "Lop chu nhiem: " << m_lopChuNhiem << endl;
}
int GVCN::tinhLuong() const {
	return GiaoVien::tinhLuong() + 500000;
}