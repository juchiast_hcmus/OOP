#include <iostream>
#include "PhanSo.h"
using namespace std;
PhanSo::PhanSo(): tu(0), mau(1) {}
PhanSo::PhanSo(int i): tu(i), mau(1) {}
PhanSo::PhanSo(int t, int m): tu(t), mau(m) {
    if (mau == 0) mau=1, tu=0;
    else if (mau < 0) tu=-tu, mau=-mau;
}
PhanSo::PhanSo(const PhanSo &p): tu(p.tu), mau(p.mau) {}
void PhanSo::Xuat() { cout << tu << '/' << mau << endl; }
PhanSo PhanSo::Cong(const PhanSo &p) {
    return PhanSo(tu*p.mau + mau*p.tu, mau*p.mau);
}
