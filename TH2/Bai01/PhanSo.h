#ifndef PHANSO_H
#define PHANSO_H
class PhanSo {
    int tu, mau;
    public:
    PhanSo();
    PhanSo(int);
    PhanSo(int, int);
    PhanSo(const PhanSo &);
    void Xuat();
    PhanSo Cong(const PhanSo &);
};
#endif

