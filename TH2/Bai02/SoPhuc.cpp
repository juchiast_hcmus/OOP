#include <iostream>
#include "SoPhuc.h"
using namespace std;
SoPhuc::SoPhuc(): thuc(0), ao(0) {}
SoPhuc::SoPhuc(double t): thuc(t), ao(0) {}
SoPhuc::SoPhuc(double t, double a): thuc(t), ao(a) {}
SoPhuc::SoPhuc(const SoPhuc &s): thuc(s.thuc), ao(s.ao) {}
void SoPhuc::Xuat() { cout << thuc << " + " << ao << "i" << endl; }
