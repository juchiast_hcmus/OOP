#include <iostream>
#include "MangSoNguyen.h"
using namespace std;
MangSoNguyen::MangSoNguyen(): size(0), data(nullptr) {}
MangSoNguyen::MangSoNguyen(size_t s): size(s), data(new int[s]) {
    for (size_t i=0; i<size; i++) data[i] = 0;
}
MangSoNguyen::MangSoNguyen(int *d, size_t s): size(s), data(new int[s]) {
    for (size_t i=0; i<size; i++) data[i] = d[i];
}
MangSoNguyen::MangSoNguyen(const MangSoNguyen &m): size(m.size), data(new int[m.size]) {
    for (size_t i=0; i<size; i++) data[i] = m.data[i];
}
void MangSoNguyen::Xuat() {
    for (size_t i=0; i<size; i++) cout << data[i] << ", ";
    cout << endl;
}
