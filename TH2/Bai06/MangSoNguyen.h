#ifndef MANGSONGUYEN_H
#define MANGSONGUYEN_H
#include <cstdint>
using std::size_t;

class MangSoNguyen {
    size_t size;
    int *data;
    public:
    MangSoNguyen();
    MangSoNguyen(size_t);
    MangSoNguyen(int *, size_t);
    MangSoNguyen(const MangSoNguyen &);
    void Xuat();
};
#endif

