#include <iostream>
#include "TamGiac.h"
using namespace std;
TamGiac::TamGiac(): a(0,0), b(1, 0), c(0,1) {}
TamGiac::TamGiac(const Diem &a, const Diem &b, const Diem &c): a(a), b(b), c(c) {}
TamGiac::TamGiac(double xa, double ya, double xb, double yb, double xc, double yc)
    :a(xa, ya), b(xb, yb), c(xc, yc) {}
TamGiac::TamGiac(const TamGiac &t): a(t.a), b(t.b), c(t.c) {}
void TamGiac::Xuat() {
    a.Xuat();
    cout << ',';
    b.Xuat();
    cout << ',';
    c.Xuat();
    cout << endl;
}
