#include <iostream>
#include "Diem.h"
using namespace std;
Diem::Diem(): x(0), y(0) {}
Diem::Diem(double x, double y): x(x), y(y) {}
Diem::Diem(const Diem &d): x(d.x), y(d.y) {}
void Diem::Xuat() { cout << "(" << x << "," << y << ")"; }
