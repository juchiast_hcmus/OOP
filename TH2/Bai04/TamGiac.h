#ifndef TAMGIAC_H
#define TAMGIAC_H
#include "Diem.h"
class TamGiac {
    Diem a, b, c;
    public:
    TamGiac();
    TamGiac(const Diem &, const Diem &, const Diem &);
    TamGiac(double, double, double, double, double, double);
    TamGiac(const TamGiac &);
    void Xuat();
};
#endif

