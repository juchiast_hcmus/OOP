#include <iostream>
#include "SinhVien.h"
using namespace std;
SinhVien::SinhVien(): ngay_sinh(1,1,1), dth(0), dlt(0) {}
SinhVien::SinhVien(const std::string &mssv): mssv(mssv), ngay_sinh(1,1,1), dth(0), dlt(0) {}
SinhVien::SinhVien(const std::string &m, const std::string &t): mssv(m), ten(t), ngay_sinh(1,1,1), dth(0), dlt(0) {}
SinhVien::SinhVien(const std::string &m, const std::string &t,
        double lt, double th): mssv(m), ten(t), ngay_sinh(1,1,1), dth(th), dlt(lt) {}
SinhVien::SinhVien(const std::string &m, const std::string &t,
        const NgaySinh &n): mssv(m), ten(t), ngay_sinh(n), dth(0), dlt(0) {}
SinhVien::SinhVien(const std::string &m, const std::string &t,
        const NgaySinh &n,
        double lt, double th): mssv(m), ten(t), ngay_sinh(n), dth(th), dlt(lt) {}
SinhVien::SinhVien(const std::string &m, const std::string &t,
        int n, int thang, int ng,
        double lt, double th): mssv(m), ten(t), ngay_sinh(n, thang, ng), dth(th), dlt(lt) {}
SinhVien::SinhVien(const SinhVien &s): mssv(s.mssv), ten(s.ten), ngay_sinh(s.ngay_sinh), dth(s.dth), dlt(s.dlt) {}
void SinhVien::Xuat() {
    cout << "MSSV: " << mssv << endl;
    cout << "Ten: " << ten << endl;
    cout << "Ngay sinh: ";
    ngay_sinh.Xuat();
    cout << "Diem li thuyet: " << dlt << endl;
    cout << "Diem thuc hanh: " << dth << endl;
    cout << endl;
}
