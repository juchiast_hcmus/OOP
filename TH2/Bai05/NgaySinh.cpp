#include <iostream>
#include "NgaySinh.h"
using namespace std;
int so_ngay(int t, int n) {
    if (t==1||t==3||t==5||t==7||t==8||t==10||t==12) return 31;
    if (t==2) {
        if (n%400==0 || (n%4==0 && n%100!=0)) return 29;
        return 28;
    }
    return 30;

}
NgaySinh::NgaySinh(int n, int t, int ng): ngay(ng), thang(t), nam(n) {
    if (thang < 1 || thang > 12) thang = 1;
    if (ngay < 1) ngay = 1;
    if (ngay > so_ngay(thang, nam)) ngay = 1;
}
NgaySinh::NgaySinh(const NgaySinh &n): ngay(n.ngay), thang(n.thang), nam(n.nam) {}
void NgaySinh::Xuat() { cout<<ngay<<'/'<<thang<<'/'<<nam<<endl;}
