#ifndef SINHVIEN_H
#define SINHVIEN_H
#include <string>
#include "NgaySinh.h"
class SinhVien {
    std::string mssv, ten;
    NgaySinh ngay_sinh;
    double dth, dlt;
    public:
    SinhVien();
    SinhVien(const std::string &mssv);
    SinhVien(const std::string &mssv, const std::string &ten);
    SinhVien(const std::string &mssv, const std::string &ten,
            double dlt, double dth);
    SinhVien(const std::string &mssv, const std::string &ten,
            const NgaySinh &);
    SinhVien(const std::string &mssv, const std::string &ten,
            const NgaySinh &,
            double dlt, double dth);
    SinhVien(const std::string &mssv, const std::string &ten,
            int, int, int,
            double dlt, double dth);
    SinhVien(const SinhVien &);
    void Xuat();
};
#endif

