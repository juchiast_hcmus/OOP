#ifndef NGAY_H
#define NGAY_H
class Ngay {
    int ngay, thang, nam;
    public:
    Ngay();
    Ngay(int);
    Ngay(int, int);
    Ngay(int, int, int);
    Ngay(const Ngay &);
    void Xuat();
};
#endif

