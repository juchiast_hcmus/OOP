#include <iostream>
#include "Ngay.h"
using namespace std;
Ngay::Ngay(): ngay(1), thang(1), nam(1) {}
Ngay::Ngay(int n): ngay(1), thang(1), nam(n) {}
Ngay::Ngay(int n, int t): ngay(1), thang(t), nam(n) {
    if (thang < 1 || thang > 12) thang = 1;
}
int so_ngay(int t, int n) {
    if (t==1||t==3||t==5||t==7||t==8||t==10||t==12) return 31;
    if (t==2) {
        if (n%400==0 || (n%4==0 && n%100!=0)) return 29;
        return 28;
    }
    return 30;

}
Ngay::Ngay(int n, int t, int ng): ngay(ng), thang(t), nam(n) {
    if (thang < 1 || thang > 12) thang = 1;
    if (ngay < 1) ngay = 1;
    if (ngay > so_ngay(thang, nam)) ngay = 1;
}
Ngay::Ngay(const Ngay &n): ngay(n.ngay), thang(n.thang), nam(n.nam) {}
void Ngay::Xuat() { cout<<ngay<<'/'<<thang<<'/'<<nam<<endl;}
