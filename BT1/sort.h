#include <iterator>

template<class T>
bool less(const T &a, const T &b) {
    return a < b;
}

template<class T>
bool greater(const T &a, const T &b) {
    return a > b;
}

// Sort array in range [first, last) using `comp` as
// comparator.
// First version use `less` as comparator.
template <class Iterator>
void sort(Iterator first, Iterator last) {
    for (Iterator i = first; i < last; i++) {
        for (Iterator j = i + 1; j < last; j++) {
            if (!less(*i, *j)) std::swap(*i, *j);
        }
    }
}
template <class Iterator, class Compare>
void sort(Iterator first, Iterator last, Compare comp) {
    for (Iterator i = first; i < last; i++) {
        for (Iterator j = i + 1; j < last; j++) {
            if (!comp(*i, *j)) std::swap(*i, *j);
        }
    }
}
