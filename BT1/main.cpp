#include <vector>
#include "phan_so.h"
#include "sort.h"
#include "print.h"

using std::vector;
using std::cout;
using std::endl;

bool custom_comparator(const PhanSo &a, const PhanSo &b) {
    return a.mau > b.mau;
}

void test_phan_so() {
    vector<PhanSo> array = {PhanSo(1, 2), PhanSo(-2, 3), PhanSo(2, 2), PhanSo(100, 35), PhanSo(20, 4)};

    cout << "Mang khi chua sap xep:" << endl;
    print(array.begin(), array.end());

    sort(array.begin(), array.end());
    cout << "Mang khi sap xep tang:" << endl;
    print(array.begin(), array.end());

    sort(array.begin(), array.end(), greater<PhanSo>);
    cout << "Mang khi sap xep giam:" << endl;
    print(array.begin(), array.end());

    sort(array.begin(), array.end(), custom_comparator);
    cout << "Sap xep mau so giam dan:" << endl;
    print(array.begin(), array.end());
}

void test_int() {
    int array[] = {4, -2, 0, 1, 2, 2, 1, 33, 1, 2};
    int length = sizeof(array) / sizeof(*array);

    cout << "Mang khi chua sap xep:" << endl;
    print(array, array + length);

    sort(array, array + length);
    cout << "Mang khi sap xep tang:" << endl;
    print(array, array + length);

    sort(array, array + length, greater<int>);
    cout << "Mang khi sap xep giam:" << endl;
    print(array, array + length);

    sort(array, array + 4);
    cout << "Sap tang 4 phan tu dau cua mang:" << endl;
    print(array, array + length);
}

void test_double() {
    double array[] = {1.22, 98.23, -23, -0.1998, -1.2};
    int length = sizeof(array) / sizeof(*array);

    cout << "Mang khi chua sap xep:" << endl;
    print(array, array + length);

    sort(array, array + length);
    cout << "Mang khi sap xep tang:" << endl;
    print(array, array + length);

    sort(array, array + length, greater<double>);
    cout << "Mang khi sap xep giam:" << endl;
    print(array, array + length);
}

int main() {
    test_phan_so();
    cout << endl;
    test_int();
    cout << endl;
    test_double();
    return 0;
}
