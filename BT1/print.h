#include <iostream>

// Print array from first to last
template<class Iterator>
void print(Iterator first, Iterator last) {
    for (Iterator i = first; i < last; i++) {
        std::cout << *i << ", ";
    }
    std::cout << std::endl;
}
