#include <iostream>

class PhanSo {
    public:
    int tu, mau;
    PhanSo(int tu, int mau): tu(tu), mau(mau) {}
};

bool operator < (const PhanSo &a, const PhanSo &b) {
    return (double)a.tu / (double)a.mau < (double)b.tu / (double)b.mau;
}

bool operator > (const PhanSo &a, const PhanSo &b) {
    return (double)a.tu / (double)a.mau > (double)b.tu / (double)b.mau;
}

std::ostream& operator << (std::ostream &os, const PhanSo &x) {
    return os << x.tu << "/" << x.mau;
}
