#ifndef TAI_KHOAN_H
#define TAI_KHOAN_H
class TaiKhoan {
private:
    float m_soDu;
public:
    float baoSoDu() { return m_soDu; }
    void napTien(float soTien) { m_soDu += soTien; }
    void rutTien(float soTien) {
        if (soTien <= m_soDu) m_soDu -= soTien;
    }
};
#endif
