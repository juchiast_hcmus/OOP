#ifndef TAI_KHOAN_TIET_KIEM_H
#define TAI_KHOAN_TIET_KIEM_H
#include "TaiKhoan.h"
template<class T>
T min(T a, T b) {
    if (a < b) return a;
    else return b;
}
class TaiKhoanTietKiem: public TaiKhoan {
private:
    float m_laiSuat;
    int m_soThangGui;
    int m_kiHanGui;
public:
    float napTien(float soTien) {
        float lai = tienLai();
        TaiKhoan::napTien(soTien);
        m_soThangGui = 0;
        return lai;
    }
    float rutTien(float soTien) {
        float lai = tienLai();
        TaiKhoan::rutTien(soTien);
        m_soThangGui = 0;
        return lai;
    }
    float tienLai() {
        return baoSoDu() * m_laiSuat * min(m_kiHanGui, m_soThangGui);
    }
};
#endif
