class Xe {
private:
    float m_xang;
    float m_hang;
public:
    Xe() {
        m_xang = 0.0;
        m_hang = 0.0;
    }
    // Do Hao xang khi chay 1km
    float doHaoXang() { return 0; }
    // Do Hao xang them khi chay 1km, cho them 1kg hang
    float doHaoXangKhiChoHang() { return 0; }
    void chay(float km) {
        m_xang -= km*doHaoXang() + km*m_hang*doHaoXangKhiChoHang();
        if (m_xang < 0.0) m_xang = 0.0;
    }
    void doXang(float lit) {
        m_xang += lit;
    }
    void themHang(float kg) {
        m_hang += kg;
    }
    void boHang(float kg) {
        m_hang -= kg;
        if (m_hang < 0.0) m_hang = 0.0;
    }
    bool daHetXang() {
        return !(m_xang >= 0.0);
    }
    float luongXangCon() {
        return m_xang;
    }
};

class XeMay: public Xe {
public:
    XeMay() { Xe(); }
    float doHaoXang() { return 2.0/100.0; }  
    float doHaoXangKhiChoHang() { return 0.1/10.0; }  
};

class XeTai: public Xe {
public:
    XeTai() { Xe(); }
    float doHaoXang() { return 20.0/100.0; }  
    float doHaoXangKhiChoHang() { return 1.0/1000.0; }  
};

int main() { return 0; }
