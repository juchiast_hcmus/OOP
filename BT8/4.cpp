class Boi {
public:
    virtual void boi() = 0;
};
class Bo {
public:
    virtual void bo() = 0;
};
class DeTrung {
public:
    virtual void* deTrung() = 0;
};
class Bay {
public:
    virtual void bay() = 0;
};
class AnTap {
public:
    virtual void anTap() = 0;
};
class DeCon {
public:
    virtual void* deCon() = 0;
};

class X : virtual public Boi {};
class Y : virtual public Bay {};
class Z : virtual public DeCon {};
class U : virtual public Boi, virtual public DeTrung {};
class V : virtual public Bo, virtual public AnTap, virtual public DeTrung {};

class CaMap : public V {};
class CaChep : public U {};
class SuTu : public Z {};
class ConBo : public Z {};
class CaVoi : public X, public Z {};
class CaSau : public U, public V {};
class TacKe : public U {};
class Doi : public Y, public Z {};

int main() {}
