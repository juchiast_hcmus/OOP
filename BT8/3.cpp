#include <iostream>
using namespace std;

class DongVat {
    public:
    virtual const char * ten() const = 0;
    virtual int toc_do() const = 0;
};

#define IMPL(class_name, name, speed) class class_name : public DongVat {\
    public:\
    const char * ten() const { return name; }\
    int toc_do() const { return speed; }\
};

IMPL(Bao, "bao", 100)
IMPL(LinhDuong, "linh duong", 80)
IMPL(SuTu, "su tu", 70)
IMPL(Cho, "cho", 60)
IMPL(Nguoi, "nguoi", 30)
IMPL(Ngua, "ngua", 60)

void so_sanh(const DongVat *a, const DongVat *b) {
    if (a->toc_do() < b->toc_do()) {
        cout << a->ten() << " chay cham hon " << b->ten() << endl;
    } else if (a->toc_do() > b->toc_do()) {
        cout << a->ten() << " chay nhanh hon " << b->ten() << endl;
    } else {
        cout << a->ten() << " chay nhanh bang " << b->ten() << endl;
    }
}


int main() {
    so_sanh(new Bao(), new Nguoi());
    so_sanh(new Ngua(), new Cho());
    return 0;
}
