#pragma once
#include "NhanVien.h"
class CongTy
{
	int so_nhan_vien;
	NhanVien** nhan_vien;
public:
	CongTy();
	CongTy(const CongTy &);
	~CongTy();
	void nhap_thong_tin();
	void xuat_thong_tin() const;
	int tong_tien_luong() const;
	NhanVien *tim_nv_luong_cao_nhat();
        int so_nv_cong_nhat() const;
        int so_nv_san_xuat() const;
        double luong_trung_binh() const;
        void liet_ke_luong_thap_hon(int) const;
        void tim_theo_ma(const string &) const;
        void tim_theo_ten(const string &) const;
        int so_nv_sinh_trong_thang_5() const;
};
