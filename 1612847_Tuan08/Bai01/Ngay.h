#pragma once
#include <iostream>
struct Ngay {
	int ngay, thang, nam;
	Ngay();
	Ngay(int, int, int);
	friend std::ostream& operator << (std::ostream &, const Ngay &);
	friend std::istream& operator >> (std::istream &, Ngay &);
};

