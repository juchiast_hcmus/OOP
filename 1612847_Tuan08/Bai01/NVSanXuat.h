#pragma once
#include "NhanVien.h"
#include "Ngay.h"
#include <string>
using std::string;
class NVSanXuat :
	public NhanVien
{
	int so_san_pham;
public:
	NVSanXuat();
	int tinh_luong() const;
	void nhap_thong_tin();
	void xuat_thong_tin() const;
	NhanVien* clone() const;
};
