#pragma once
#include <string>
#include "Ngay.h"
using std::string;
class NhanVien
{
	string ma_nhan_vien;
	string ho_ten;
	string dia_chi;
	Ngay ngay_sinh;
public:
	NhanVien();
        virtual ~NhanVien();
	virtual void xuat_thong_tin() const;
	virtual void nhap_thong_tin();
	virtual int tinh_luong() const = 0;
	virtual NhanVien* clone() const = 0;
        const string& lay_ma() const;
        const Ngay& lay_ngay_sinh() const;
        const string& lay_ten() const;
};
