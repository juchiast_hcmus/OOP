#include "NVCongNhat.h"
#include <iostream>

NVCongNhat::NVCongNhat() : NhanVien(), so_ngay_lam(0) {}
int NVCongNhat::tinh_luong() const {
	return 300000 * so_ngay_lam;
}

void NVCongNhat::nhap_thong_tin() {
	using std::cin;
	using std::cout;
	using std::endl;
	NhanVien::nhap_thong_tin();
	cout << "So ngay lam: ";
	cin >> so_ngay_lam;
	cin.ignore();
}

void NVCongNhat::xuat_thong_tin() const {
	using std::cout;
	using std::endl;
	NhanVien::xuat_thong_tin();
	cout << "So ngay lam: " << so_ngay_lam << endl;
	cout << "Luong: " << tinh_luong() << endl;
}

NhanVien* NVCongNhat::clone() const {
	return new NVCongNhat(*this);
}