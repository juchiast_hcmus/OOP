#include "CongTy.h"
#include "NVCongNhat.h"
#include "NVSanXuat.h"
#include <iostream>

CongTy::CongTy() : so_nhan_vien(0), nhan_vien(nullptr) {}

CongTy::CongTy(const CongTy &ct) {
    nhan_vien = nullptr;
    so_nhan_vien = ct.so_nhan_vien;
    if (so_nhan_vien > 0) {
        nhan_vien = new NhanVien*[so_nhan_vien];
        for (int i = 0; i < so_nhan_vien; i++) {
            nhan_vien[i] = ct.nhan_vien[i]->clone();
        }
    }
}

CongTy::~CongTy() {
    for (int i = 0; i < so_nhan_vien; i++) delete nhan_vien[i];
    delete[] nhan_vien;
}

void CongTy::nhap_thong_tin() {
    using namespace std;

    cout << "Nhap so nhan vien: ";
    cin >> so_nhan_vien; cin.ignore();
    nhan_vien = new NhanVien*[so_nhan_vien];
    for (int i = 0; i < so_nhan_vien; i++) {
        cout << "Nhap loai nhan vien (0: NV san xuat, 1: NV cong nhat): ";
        int t; cin >> t; cin.ignore();
        if (t) {
            NVCongNhat *nv = new NVCongNhat();
            nv->nhap_thong_tin();
            nhan_vien[i] = nv;
        }
        else {
            NVSanXuat *nv = new NVSanXuat();
            nv->nhap_thong_tin();
            nhan_vien[i] = nv;
        }
    }
}

void CongTy::xuat_thong_tin() const {
    using namespace std;

    cout << "So nhan vien: " << so_nhan_vien << endl;
    for (int i = 0; i < so_nhan_vien; i++) {
        nhan_vien[i]->xuat_thong_tin();
        cout << "-------------------";
    }
    cout << endl;
}

int CongTy::tong_tien_luong() const {
    int tong = 0;
    for (int i = 0; i < so_nhan_vien; i++) tong += nhan_vien[i]->tinh_luong();
    return tong;
}

NhanVien* CongTy::tim_nv_luong_cao_nhat() {
    if (so_nhan_vien < 1) return nullptr;
    NhanVien *nv = nhan_vien[0];
    int luong = nv->tinh_luong();
    for (int i = 1; i < so_nhan_vien; i++) {
        if (nhan_vien[i]->tinh_luong() > luong) {
            luong = nhan_vien[i]->tinh_luong();
            nv = nhan_vien[i];
        }
    }
    return nv;
}

int CongTy::so_nv_san_xuat() const {
    int count = 0;
    for (int i=0; i < so_nhan_vien; i++) {
        if (dynamic_cast<NVSanXuat*>(nhan_vien[i])) count++;
    }
    return count;
}

int CongTy::so_nv_cong_nhat() const {
    int count = 0;
    for (int i=0; i < so_nhan_vien; i++) {
        if (dynamic_cast<NVCongNhat*>(nhan_vien[i])) count++;
    }
    return count;
}

double CongTy::luong_trung_binh() const {
    return (double)tong_tien_luong() / (double)so_nhan_vien;
}

void CongTy::liet_ke_luong_thap_hon(int k) const {
    using namespace std;
    for (int i=0; i<so_nhan_vien; i++) {
        if (nhan_vien[i]->tinh_luong() < k) {
            nhan_vien[i]->xuat_thong_tin();
            cout << "-------------------" << endl;
        }
    }
    cout << endl;
}

void CongTy::tim_theo_ma(const string &ma) const {
    using namespace std;
    for (int i=0; i<so_nhan_vien; i++) {
        if (nhan_vien[i]->lay_ma() == ma) {
            nhan_vien[i]->xuat_thong_tin();
            cout << "-------------------" << endl;
        }
    }
    cout << endl;
}

void CongTy::tim_theo_ten(const string &ten) const {
    using namespace std;
    for (int i=0; i<so_nhan_vien; i++) {
        if (nhan_vien[i]->lay_ten() == ten) {
            nhan_vien[i]->xuat_thong_tin();
            cout << "-------------------" << endl;
        }
    }
    cout << endl;
}

int CongTy::so_nv_sinh_trong_thang_5() const {
    int count = 0;
    for (int i=0; i<so_nhan_vien; i++) {
        if (nhan_vien[i]->lay_ngay_sinh().thang == 5) count++;
    }
    return count;
}
