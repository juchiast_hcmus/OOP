// 1612847
// Do Hoang Anh Duy
// Bai 01

#include "CongTy.h"
#include <iostream>

int main() {
	using namespace std;
	CongTy x;
	x.nhap_thong_tin();
	x.xuat_thong_tin();
	cout << endl << "Tong tien luong: " << x.tong_tien_luong() << endl;
	NhanVien *nv = x.tim_nv_luong_cao_nhat();
	if (nv) {
		cout << "Nhan vien co luong cao nhat: " << endl;
		nv->xuat_thong_tin();
	}
        cout << "So NVSanXuat: " << x.so_nv_san_xuat() << endl;
        cout << "So NVCongNhat: " << x.so_nv_cong_nhat() << endl;
        cout << "Luong trung binh: " << x.luong_trung_binh() << endl;
        cout << "Cac nhan vien luong < 3.000.000:" << endl;
        x.liet_ke_luong_thap_hon(3000000);
        string str;
        cout << "Nhap ma nhan vien can tim: "; getline(cin, str);
        cout << "Ket qua tim:" << endl;
        x.tim_theo_ma(str);
        cout << "Nhap ten nhan vien can tim: "; getline(cin, str);
        cout << "Ket qua tim:" << endl;
        x.tim_theo_ten(str);
        cout << "So nhan vien sinh thang 5: " << x.so_nv_sinh_trong_thang_5() << endl;
        return 0;
}
