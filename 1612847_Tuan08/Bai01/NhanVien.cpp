#include "NhanVien.h"
#include <iostream>

NhanVien::NhanVien() {}
NhanVien::~NhanVien() {}

void NhanVien::xuat_thong_tin() const {
	using std::cout;
	using std::endl;
	cout << endl;
	cout << "Ma nhan vien: " << ma_nhan_vien << endl;
	cout << "Ho ten: " << ho_ten << endl;
	cout << "Dia chi: " << dia_chi << endl;
	cout << "Ngay sinh: " << ngay_sinh << endl;
}

void NhanVien::nhap_thong_tin() {
	using std::cin;
	using std::cout;
	using std::endl;
	cout << "Nhap thong tin nhan vien: " << endl;
	cout << "Ma nhan vien: ";
	getline(cin, ma_nhan_vien);
	cout << "Ho ten: ";
	getline(cin, ho_ten);
	cout << "Dia chi: ";
	getline(cin, dia_chi);
	cout << "Ngay sinh: ";
	cin >> ngay_sinh;
	cin.ignore();
}

const string &NhanVien::lay_ma() const { return ma_nhan_vien; }
const Ngay &NhanVien::lay_ngay_sinh() const { return ngay_sinh; }
const string &NhanVien::lay_ten() const { return ho_ten; }
