#include "NVSanXuat.h"
#include <iostream>

NVSanXuat::NVSanXuat() : NhanVien(), so_san_pham(0) {}
int NVSanXuat::tinh_luong() const {
	return 20000 * so_san_pham;
}

void NVSanXuat::nhap_thong_tin() {
	using std::cin;
	using std::cout;
	using std::endl;
	NhanVien::nhap_thong_tin();
	cout << "So san pham lam duoc: ";
	cin >> so_san_pham;
	cin.ignore();
}

void NVSanXuat::xuat_thong_tin() const {
	using std::cout;
	using std::endl;
	NhanVien::xuat_thong_tin();
	cout << "So san pham: " << so_san_pham << endl;
	cout << "Luong: " << tinh_luong() << endl;
}

NhanVien* NVSanXuat::clone() const {
	return new NVSanXuat(*this);
}