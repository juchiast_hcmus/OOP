#pragma once
#include "Basic.h"

class DataFix: public Basic {
    int cuoc_dien_thoai(int thoi_gian_goi) const {
        return Basic::cuoc_dien_thoai(thoi_gian_goi) * 9 / 10;
    }
    int cuoc_internet(int) const {
        return 1000000;
    }
    public:
    DataFix(): Basic() {}
};
