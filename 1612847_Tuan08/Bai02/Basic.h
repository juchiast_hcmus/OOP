#pragma once
#include <string>
#include <iostream>
using namespace std;

class Basic {
    string ten, cmnd, diachi;
    protected:
    virtual int cuoc_dien_thoai(int luu_luong) const {
        return luu_luong * 200;
    }
    virtual int cuoc_internet(int thoi_gian_goi) const {
        return thoi_gian_goi * 1000;
    }
    public:
    Basic() {
        cout << "Nhap ten: ";
        getline(cin, ten);
        cout << "Nhap cmnd: ";
        getline(cin, cmnd);
        cout << "Nhap dia chi: ";
        getline(cin, diachi);
    }
    virtual ~Basic() {}
    virtual int tinh_cuoc_tong(int luu_luong, int thoi_gian_goi) const {
        return (cuoc_dien_thoai(thoi_gian_goi) + cuoc_internet(luu_luong)) * 11 / 10;
    }
    void xuat() const {
        cout << "Ten: " << ten << endl
            << "Cmnd: " << cmnd << endl
            << "Dia chi: " << diachi << endl;
    }
};
