#pragma once
#include <vector>
#include "Basic.h"
#include "DataFree.h"
#include "DataFix.h"

class CongTy {
    std::vector<Basic *> hop_dong;
    public:
    CongTy() {}
    CongTy(const CongTy &c) {
        for (Basic *h: c.hop_dong) {
            if (dynamic_cast<DataFix*>(h))
                hop_dong.push_back(new DataFix(*dynamic_cast<DataFix*>(h)));
            else if (dynamic_cast<DataFree*>(h))
                hop_dong.push_back(new DataFree(*dynamic_cast<DataFree*>(h)));
            else 
                hop_dong.push_back(new Basic(*h));
        }
    }
    void ky_hop_dong() {
        cout << "Chon loai hop dong (1: Basic, 2: DataFree, 3: DataFix): ";
        int type;
        cin >> type; cin.ignore();
        if (type == 1) hop_dong.push_back(new Basic());
        if (type == 2) hop_dong.push_back(new DataFree());
        if (type == 3) hop_dong.push_back(new DataFix());
    }
    void tinh_cuoc() const {
        int i = 0;
        for (auto h: hop_dong) {
            int luu_luong, thoi_gian_goi;
            cout << "Nhap luu luong va thoi thoi gian goi cho khach hang thu " << i << ": ";
            cin >> luu_luong >> thoi_gian_goi; cin.ignore();
            h->xuat();
            cout << "Cuoc: " << h->tinh_cuoc_tong(luu_luong, thoi_gian_goi) << endl;
            i++;
        }
    }
};
