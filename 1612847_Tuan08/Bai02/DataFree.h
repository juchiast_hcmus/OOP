#pragma once
#include "Basic.h"

class DataFree: public Basic {
    int nguong_mien_phi;
    int cuoc_internet(int luu_luong) const {
        if (luu_luong <= nguong_mien_phi) return 0;
        return Basic::cuoc_internet(luu_luong - nguong_mien_phi);
    }
    public:
    DataFree(): Basic() {
        cout << "Nhap nguong luu luong: ";
        cin >> nguong_mien_phi; cin.ignore();
    }
};
