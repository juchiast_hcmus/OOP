#include "Obstacle.h"
#include "Image.h"

namespace obstacle {
	Obstacle::Obstacle(object::Rectangle r, int id, Direction d, int s)
		: object::Object(r, id), direction(d), speed(s) {}

	void Obstacle::move() {
		switch (direction) {
		case Direction::Left:
			Object::move(object::Direction::Left, speed);
			break;
		case Direction::Right:
			Object::move(object::Direction::Right, speed);
			break;
		}
	}

	void Obstacle::rmove() {
		switch (direction) {
		case Direction::Left:
			Object::rmove(object::Direction::Left, speed);
			break;
		case Direction::Right:
			Object::rmove(object::Direction::Right, speed);
			break;
		}
	}

	void Obstacle::move_into_board(int width, int height) {
		if (rect.low_y() > height) rect.y -= rect.low_y() - height;
		if (rect.low_x() > width) rect.x -= rect.low_x() - width;
	}

	bool Obstacle::may_conflict(const Obstacle *x) const {
		if (direction != x->direction) {
			return !(rect.low_y() < x->rect.y || rect.y > x->rect.low_y());
		} else {
			return conflict(x);
		}
	}

	void Obstacle::draw(Gdiplus::Graphics *g) const {
		image::draw(image_id, g, rect.x, rect.y, direction == Direction::Right);
	}

	Car::Car(int x, int y, Direction d, int s)
		: Obstacle({ x, y, image::width(1), image::height(1)}, 1, d, s) {}

	Cat::Cat(int x, int y, Direction d, int s)
		: Obstacle({ x, y, image::width(3), image::height(3) }, 3, d, s) {}

	Pig::Pig(int x, int y, Direction d, int s)
		: Obstacle({ x, y, image::width(4), image::height(4) }, 4, d, s) {}

	Truck::Truck(int x, int y, Direction d, int s)
		: Obstacle({ x, y, image::width(2), image::height(2) }, 2, d, s) {}

	Worm::Worm(int x, int y, Direction d, int s)
		: Obstacle({ x, y, image::width(5), image::height(5) }, 5, d, s) {}

	Obstacle* random(int w, int h, int speed) {
		const int BOT_COUNT = 5;
		Direction d = (rand() & 1) ? Direction::Left : Direction::Right;
		int x = d == Direction::Left ? w : 0;
		int y = rand() % h;
		int type = rand() % BOT_COUNT;
		Obstacle *bot = nullptr;
		switch (type) {
		case 0:
			bot = new Car(x, y, d, speed);
			break;
		case 1:
			bot = new Cat(x, y, d, speed);
			break;
		case 2:
			bot = new Pig(x, y, d, speed);
			break;
		case 3:
			bot = new Truck(x, y, d, speed);
			break;
		case 4:
			bot = new Worm(x, y, d, speed);
			break;
		default:
			break;
		}
		bot->move_into_board(w, h);
		return bot;
	}
};