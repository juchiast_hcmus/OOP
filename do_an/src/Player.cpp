#include "Player.h"
#include "Image.h"

namespace player {
	Player::Player(int x, int y, int speed)
		: Object({ x, y, image::width(0), image::height(0) }, 0), speed(speed) {}

	void Player::move(object::Direction d) {
		Object::move(d, speed);
	}
	void Player::rmove(object::Direction d) {
		Object::rmove(d, speed);
	}
};