#include "Object.h"
#include "Image.h"

template<class T>
bool inside(T l, T i, T r) {
	return l <= i && i <= r;
}

namespace object {
	int Rectangle::low_x() const {
		return x + width;
	}

	int Rectangle::low_y() const {
		return y + height;
	}

	Object::Object() {}
	Object::~Object() {}

	Object::Object(Rectangle r, int id) : rect(r), image_id(id) {
	}

	bool Object::conflict(const Object *o) const {
		return (max(rect.x, o->rect.x) < min(rect.low_x(), o->rect.low_x()))
			&& (max(rect.y, o->rect.y) < min(rect.low_y(), o->rect.low_y()));
	}

	void Object::move(Direction direction, int distance) {
		switch (direction) {
		case Direction::Left:
			rect.x -= distance;
			break;
		case Direction::Right:
			rect.x += distance;
			break;
		case Direction::Up:
			rect.y -= distance;
			break;
		case Direction::Down:
			rect.y += distance;
			break;
		}
	}

	void Object::rmove(Direction direction, int distance) {
		move(direction, -distance);
	}

	void Object::draw(Gdiplus::Graphics *g) const {
		image::draw(image_id, g, rect.x, rect.y);
	}

	bool Object::in_board(int w, int h) const {
		return inside(0, rect.x, w) && inside(0, rect.y, h)
			&& inside(0, rect.low_x(), w) && inside(0, rect.low_y(), h);
	}
};