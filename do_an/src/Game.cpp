#include "Game.h"
#include <thread>
#include <Windows.h>
#include <conio.h>
#include <iostream>
using std::cout;

static const int PLAYER_SPEED = 4;

namespace game {
	void thread_func(Game *g) {
		while (1) {
			char key = _getch();
			switch (key) {
			case 'a':
				g->emit(Event::Left);
				break;
			case 'd':
				g->emit(Event::Right);
				break;
			case 's':
				g->emit(Event::Down);
				break;
			case 'w':
				g->emit(Event::Up);
				break;
			case '1':
				g->emit(Event::One);
				break;
			case '2':
				g->emit(Event::Two);
				break;
			case '3':
				g->emit(Event::Three);
				break;
			case 'y':
				g->emit(Event::Yes);
				break;
			case 'n':
				g->emit(Event::No);
				break;
			default:
				break;
			}
		}
	}

	void Game::emit(Event e) {
		event = e;
	}

	void Game::run() {
		std::thread t1(thread_func, this);
		while (1) {
			switch (status)
			{
			case game::Status::StartMenu:
				system("cls");
				cout << "1. New game\n2. Load Game\n3. Load game\n";
				switch (event)
				{
				case game::Event::One:
					start_play();
					status = Status::Playing;
					break;
				case game::Event::Two:
					cout << "Not implemented!\n";
					break;
				case game::Event::Three:
					cout << "Not implemented!\n";
					break;
				default:
					break;
				}
				event = Event::None;
				std::this_thread::sleep_for(std::chrono::milliseconds(80));
				break;
			case game::Status::ReplayMenu:
				system("cls");
				cout << "Press 'y' to play again, 'n' to exit.";
				switch (event)
				{
				case game::Event::Yes:
					start_play();
					status = Status::Playing;
					break;
				case game::Event::No:
					exit(0);
					break;
				default:
					break;
				}
				std::this_thread::sleep_for(std::chrono::milliseconds(80));
			case game::Status::PauseMenu:
				break;
			case game::Status::Saving:
				break;
			case game::Status::Loading:
				break;
			case game::Status::Playing:
				switch (event)
				{
				case game::Event::Up:
					player->move(object::Direction::Up);
					if (!player->in_board(width, height)) player->rmove(object::Direction::Up);
					break;
				case game::Event::Down:
					player->move(object::Direction::Down);
					if (!player->in_board(width, height)) level_up();
					break;
				case game::Event::Left:
					player->move(object::Direction::Left);
					if (!player->in_board(width, height)) player->rmove(object::Direction::Left);
					break;
				case game::Event::Right:
					player->move(object::Direction::Right);
					if (!player->in_board(width, height)) player->rmove(object::Direction::Right);
					break;
				default:
					break;
				}
				if (rand() % (30 - level) <= 3) spawn();
				if (!next()) {
					status = Status::ReplayMenu;
				}
				render();
				std::this_thread::sleep_for(std::chrono::milliseconds(30));
				break;
			default:
				break;
			}
		}
	}

	void clear(HDC hdc, int w, int h) {
		const COLORREF BACKGROUND = RGB(0, 0, 0);
		for (int j = 0; j < h; j++) for (int i = 0; i < h; i++) {
			SetPixel(hdc, i, j, BACKGROUND);
		}
	}

	bool Game::next() {
		for (auto x : obstacles) x->move();
		obstacles.remove_if([&](const obstacle::Obstacle *x) {
			return !x->in_board(width, height);
		});
		for (auto x : obstacles) if (player->conflict(x)) return false;
		return true;
	}

	void Game::spawn() {
		const int SPEED = 6;
		obstacle::Obstacle *p = obstacle::random(width, height, SPEED);
		bool conflict = false;
		for (auto x : obstacles) if (x->may_conflict(p)) {
			conflict = true;
			break;
		}
		if (!conflict) obstacles.push_back(p);
	}

	void Game::render() {
		auto console = GetConsoleWindow();
		auto hdc = GetDC(console);
		auto g = new Gdiplus::Graphics(hdc);
		Gdiplus::SolidBrush black_brush(Gdiplus::Color(255, 0, 0, 0));
		g->FillRectangle(&black_brush, 0, 0, width + 100, height);
		player->draw(g);
		for (auto x : obstacles) x->draw(g);
		image::draw_string(L"Level: " + std::to_wstring(level), g, width, 0);
		delete g;
		ReleaseDC(console, hdc);
	}
	
	Game::Game() {
		event = Event::None;
		status = Status::StartMenu;
		player = nullptr;
	}

	void Game::start_play() {
		level = 0;
		width = 400;
		height = 200;
		delete player;
		player = new player::Player(width / 2, 0, PLAYER_SPEED);
		obstacles.clear();
	}

	void Game::level_up() {
		level++;
		delete player;
		player = new player::Player(width / 2, 0, PLAYER_SPEED);
		event = Event::None;
		obstacles.clear();
	}
};