#include "Image.h"

static Gdiplus::Font *my_font;
static Gdiplus::Image *images[12];
static ULONG_PTR gdiplusToken;

namespace image {
	void init() {
		Gdiplus::GdiplusStartupInput gdiplusStartupInput;
		Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
		images[0] = Gdiplus::Image::FromFile(L"player.png");
		images[1] = Gdiplus::Image::FromFile(L"car.png");
		images[2] = Gdiplus::Image::FromFile(L"truck.png");
		images[3] = Gdiplus::Image::FromFile(L"cat.png");
		images[4] = Gdiplus::Image::FromFile(L"pig.png");
		images[5] = Gdiplus::Image::FromFile(L"worm.png");
		for (int i = 0; i < 6; i++) {
			images[i + 6] = images[i]->Clone();
			images[i + 6]->RotateFlip(Gdiplus::Rotate180FlipY);
		}
		my_font = new Gdiplus::Font(L"Consolos", 10);
	}
	int width(int id) {
		return images[id]->GetWidth();
	}
	int height(int id) {
		return images[id]->GetHeight();
	}
	void draw(int id, Gdiplus::Graphics *g, int x, int y, bool flip) {
		if (flip) id += 6;
		g->DrawImage(images[id], x, y, width(id), height(id));
	}
	void draw_string(const std::wstring &s, Gdiplus::Graphics *g, int x, int y) {
		Gdiplus::PointF origin(x, y);
		Gdiplus::SolidBrush white_brush(Gdiplus::Color(255, 255, 255, 255));

		// Draw string.
		g->DrawString(s.data(), s.size(), my_font, origin, &white_brush);
	}
};
