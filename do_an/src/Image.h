#pragma once
#include <windows.h> 
#pragma comment(lib, "gdiplus.lib")
#include <gdiplus.h>
#include <string>

namespace image {
	void init();
	int width(int id);
	int height(int id);
	void draw(int id, Gdiplus::Graphics *, int x, int y, bool flip=false);
	void draw_string(const std::wstring &, Gdiplus::Graphics *, int x, int y);
};