#pragma once
#include "Object.h"

namespace obstacle {
	enum class Direction {
		Left,
		Right,
	};

	class Obstacle : public object::Object {
		Direction direction;
		int speed;
	public:
		Obstacle(object::Rectangle, int id, Direction, int speed);
		void move();
		void rmove();
		void move_into_board(int width, int height);
		bool may_conflict(const Obstacle *) const;
		void draw(Gdiplus::Graphics *) const;
	};

	class Car : public Obstacle {
	public:
		Car(int x, int y, Direction, int speed);
	};

	class Cat : public Obstacle {
	public:
		Cat(int x, int y, Direction, int speed);
	};

	class Pig : public Obstacle {
	public:
		Pig(int x, int y, Direction, int speed);
	};

	class Truck : public Obstacle {
	public:
		Truck(int x, int y, Direction, int speed);
	};

	class Worm : public Obstacle {
	public:
		Worm(int x, int y, Direction, int speed);
	};

	Obstacle* random(int w, int h, int speed);
};