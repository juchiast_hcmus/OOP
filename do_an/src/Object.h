#pragma once
#include "Image.h"
#include <Windows.h>

namespace object {
	struct Rectangle {
		int x, y, width, height;
		int low_x() const;
		int low_y() const;
	};

	enum class Direction {
		Left,
		Right,
		Up,
		Down,
	};

	class Object {
	protected:
		Rectangle rect;
		int image_id;
	public:
		Object();
		Object(Rectangle, int id);
		virtual ~Object();
		void move(Direction, int distance);
		void rmove(Direction, int distance);
		bool conflict(const Object *) const;
		virtual void draw(Gdiplus::Graphics *) const;
		bool in_board(int width, int height) const;
		//virtual void save() const = 0;
		//virtual void load() = 0;
	};
};