#pragma once
#include "Object.h"
namespace player {
	class Player : public object::Object {
		int speed;
	public:
		Player(int x, int y, int speed);
		void move(object::Direction);
		void rmove(object::Direction);
	};
}