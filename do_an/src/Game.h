#pragma once
#include "Player.h"
#include "Obstacle.h"
#include <list>

namespace game {
	enum class Event {
		None,
		Up,
		Down,
		Left,
		Right,
		Save,
		Load,
		Pause,
		One,
		Two,
		Three,
		Four,
		Five,
		Six,
		Yes,
		No,
	};

	enum class Status {
		StartMenu,
		ReplayMenu,
		PauseMenu,
		Saving,
		Loading,
		Playing,
	};

	class Game {
		Event event;
		player::Player *player;
		Status status;
		std::list<obstacle::Obstacle *> obstacles;
		int width, height;
		int level;

		bool next();
		void spawn();
		void render();
		void level_up();
		void start_play();
	public:
		Game();
		void emit(Event);
		void run();
		//void save() const;
		//void load();
	};
};