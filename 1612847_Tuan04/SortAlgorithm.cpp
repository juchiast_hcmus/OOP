#include <iostream>
#include <string>
using namespace std;


template<class T>
class SortAlgorithm {
    private:
    static SortAlgorithm<T>* _algorithm;
    void(*currentAlgorithm)(T[], int);
    SortAlgorithm();
    public:
    static SortAlgorithm* getObject(void(*pAlg)(T[], int));
    static void InsertionSort(T[], int);
    static void InterchangeSort(T[], int);
    void Sort(T[], int);
};
template<class T>
SortAlgorithm<T>* SortAlgorithm<T>::_algorithm = NULL;
template<class T>
SortAlgorithm<T>::SortAlgorithm() {
    currentAlgorithm = InsertionSort;
}
template<class T>
SortAlgorithm<T>* SortAlgorithm<T>::getObject(void(*pAlg)(T[], int)) {
    if (_algorithm == NULL) {
        _algorithm = new SortAlgorithm<T>();
    }
    if (pAlg != NULL) {
        _algorithm->currentAlgorithm = pAlg;
    }
    return _algorithm;
}
template<class T>
void SortAlgorithm<T>::Sort(T a[], int n) {
    if (currentAlgorithm != NULL) {
        currentAlgorithm(a, n);
    }
}
template<class T>
void SortAlgorithm<T>::InsertionSort(T a[], int n) {
    int pos;
    T x;
    for (int i = 1; i < n; i++) {
        x = a[i];
        for (pos = i; (pos > 0) && a[pos - 1] > x;pos--) {
            a[pos] = a[pos - 1];
        }
        a[pos] = x;
    }
}
template<class T>
void SortAlgorithm<T>::InterchangeSort(T a[], int n) {
    for (int i = 0; i < n - 1; i++) {
        for (int j = i + 1; j < n; j++) {
            if (a[j] < a[i]) {
                T temp = a[i];
                a[i] = a[j];
                a[j] = temp;
            }
        }
    }
}

class HOCSINH {
    string ten;
    double diem;
    public:
    HOCSINH() {}
    HOCSINH(const string &ten, double diem): ten(ten), diem(diem) {}
    bool operator < (const HOCSINH &h) const {
        return diem < h.diem;
    }
    bool operator > (const HOCSINH &h) const {
        return diem > h.diem;
    }
    friend ostream& operator << (ostream& os, HOCSINH &hs) {
        return os << "Ten: " << hs.ten << ", Diem: " << hs.diem;
    }
};

int main() {
    float a1[] = { 1.4F, -5.2F, 3.3F, 0 };
    char a2[] = { 'A', 'B', 'C', 'D' };
    HOCSINH a3[] = { HOCSINH("Lan", 3), HOCSINH("Huong", 4) };
    int n1 = sizeof(a1) / sizeof(a1[0]);
    int n2 = sizeof(a2) / sizeof(a2[0]);
    int n3 = sizeof(a3) / sizeof(a3[0]);
    SortAlgorithm<float>* alg1 =
        SortAlgorithm<float>::getObject(SortAlgorithm<float>::InterchangeSort);
    SortAlgorithm<char>* alg2 =
        SortAlgorithm<char>::getObject(SortAlgorithm<char>::InterchangeSort);
    SortAlgorithm<HOCSINH>* alg3 =
        SortAlgorithm<HOCSINH>::getObject(SortAlgorithm<HOCSINH>::InterchangeSort);
    alg1->Sort(a1, n1);
    alg2->Sort(a2, n2);
    alg3->Sort(a3, n3);
    cout << "Mang thuc sau khi sap xep tang dan:" << endl;
    for (int i = 0; i < n1; i++)
        cout << a1[i] << "\t";
    cout << endl;
    cout << "Mang ki tu sau khi sap xep tang dan:" << endl;
    for (int i = 0; i < n2; i++)
        cout << a2[i] << "\t";
    cout << endl;
    cout << "Mang HOCSINH sau khi sap xep tang dan theo diem:" << endl;
    for (int i = 0; i < n3; i++)
        cout << a3[i] << "\n";
    return 0;
}
