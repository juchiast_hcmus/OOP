#include <iostream>

class IntArray {
    int *data;
    int size;
    public:
    IntArray();
    ~IntArray();
    IntArray(int);
    IntArray(int *, int);
    IntArray(const IntArray &);
    IntArray& operator = (const IntArray &);
    int& operator [](int);
    const int& operator [](int) const;
    operator int() const;
};
std::ostream& operator << (std::ostream &, const IntArray &);

IntArray::IntArray(): data(nullptr), size(0) {}
IntArray::~IntArray() {
    if (data != nullptr) delete[] data;
}
IntArray::IntArray(int size): data(new int[size]), size(size) {
    if (data == nullptr) throw 0;
    for (int i=0; i<size; i++) data[i] = 0;
}
IntArray::IntArray(int *ptr, int size): data(new int[size]), size(size) {
    if (data == nullptr) throw 0;
    for (int i=0; i<size; i++) data[i] = ptr[i];
}
IntArray::IntArray(const IntArray &arr): data(new int[arr.size]), size(arr.size) {
    if (data == nullptr) throw 0;
    for (int i=0; i<size; i++) data[i] = arr[i];
}
IntArray& IntArray::operator=(const IntArray &arr) {
    if (data != nullptr) delete[] data;
    size = arr.size;
    data = new int[size];
    for (int i=0; i<size; i++) data[i] = arr[i];
    return *this;
}
int& IntArray::operator[](int i) {
    return data[i];
}
const int& IntArray::operator[](int i) const {
    return data[i];
}
IntArray::operator int() const {
    return size;
}
std::ostream& operator << (std::ostream &os, const IntArray &a) {
    for (int i=0; i<(int)a; i++) {
        os << a[i] << ',';
    }
    os << std::endl;
    return os;
}
std::istream& operator >> (std::istream &is, IntArray &a) {
    for (int i=0; i<(int)a; i++) {
        is >> a[i];
    }
    return is;
}
