#pragma once
#include <iostream>

class Fraction {
    int t, m;
    void standardize();
    public:
    Fraction();
    Fraction(int, int);
    Fraction(const Fraction &);
    Fraction& operator = (const Fraction &);
    Fraction operator + (const Fraction &) const;
    Fraction operator - (const Fraction &) const;
    Fraction operator * (const Fraction &) const;
    Fraction operator / (const Fraction &) const;
    bool operator == (const Fraction &) const;
    bool operator != (const Fraction &) const;
    bool operator >= (const Fraction &) const;
    bool operator > (const Fraction &) const;
    bool operator <= (const Fraction &) const;
    bool operator < (const Fraction &) const;
    Fraction operator + (int) const;
    friend Fraction operator + (int, const Fraction &);
    Fraction operator - (int) const;
    friend Fraction operator - (int, const Fraction &);
    Fraction operator * (int) const;
    friend Fraction operator * (int, const Fraction &);
    friend std::ostream& operator << (std::ostream &, const Fraction &);
    Fraction& operator += (const Fraction &);
    Fraction& operator -= (const Fraction &);
    Fraction& operator *= (const Fraction &);
    Fraction& operator /= (const Fraction &);
    Fraction& operator ++ ();
    Fraction& operator -- ();
    Fraction operator ++ (int);
    Fraction operator -- (int);
    operator float() const;
};
