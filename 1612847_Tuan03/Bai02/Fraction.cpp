#include "Fraction.h"
int gcd(int a, int b) {
    if (a < 0) a = -a;
    int r;
    while (b != 0) r = a%b, a = b, b = r;
    return a;
}
void Fraction::standardize() {
    if (m == 0) t = 0, m = 1;
    else {
        if (m < 0) t = -t, m = -m;
        int g = gcd(t, m);
        t /= g, m /= g;
    }
}
Fraction::Fraction(): t(0), m(1) {}
Fraction::Fraction(int t, int m): t(t), m(m) {
    standardize();
}
Fraction::Fraction(const Fraction &f): t(f.t), m(f.m) {}
Fraction& Fraction::operator = (const Fraction &f) {
    t = f.t;
    m = f.m;
    return *this;
}
Fraction Fraction::operator + (const Fraction &f) const {
    return Fraction(t*f.m + m*f.t, m * f.m);
}
Fraction Fraction::operator - (const Fraction &f) const {
    return Fraction(t*f.m - m*f.t, m * f.m);
}
Fraction Fraction::operator * (const Fraction &f) const {
    return Fraction(t*f.t, m * f.m);
}
Fraction Fraction::operator / (const Fraction &f) const {
    return Fraction(t*f.m, m * f.t);
}
bool Fraction::operator == (const Fraction &f) const {
    return (t == f.t) && (m == f.m);
}
bool Fraction::operator != (const Fraction &f) const {
    return !(*this == f);
}
bool Fraction::operator >= (const Fraction &f) const {
    return !(*this < f);
}
bool Fraction::operator > (const Fraction &f) const {
    return (*this != f) && (float)(*this) > (float)f;
}
bool Fraction::operator <= (const Fraction &f) const {
    return !(*this > f);
}
bool Fraction::operator < (const Fraction &f) const {
    return (*this != f) && (float)(*this) < (float)f;
}
Fraction Fraction::operator + (int i) const {
    return Fraction(t + i*m, m);
}
Fraction operator + (int i, const Fraction &f) {
    return f + i;
}
Fraction Fraction::operator - (int i) const {
    return Fraction(t - i*m, m);
}
Fraction operator - (int i, const Fraction &f) {
    return Fraction(i*f.m - f.t, f.m);
}
Fraction Fraction::operator * (int i) const {
    return Fraction(i*t, m);
}
Fraction operator * (int i, const Fraction &f) {
    return f * i;
}
std::ostream& operator << (std::ostream &os, const Fraction &f) {
    return os << f.t << '/' << f.m << std::endl;
}
Fraction& Fraction::operator += (const Fraction &f) {
    *this = *this + f;
    return *this;
}
Fraction& Fraction::operator -= (const Fraction &f) {
    *this = *this - f;
    return *this;
}
Fraction& Fraction::operator *= (const Fraction &f) {
    *this = *this * f;
    return *this;
}
Fraction& Fraction::operator /= (const Fraction &f) {
    *this = *this / f;
    return *this;
}
Fraction& Fraction::operator ++ () {
    *this = *this + 1;
    return *this;
}
Fraction& Fraction::operator -- () {
    *this = *this - 1;
    return *this;
}
Fraction Fraction::operator ++ (int) {
    Fraction tmp(*this);
    ++*this;
    return tmp;
}
Fraction Fraction::operator -- (int) {
    Fraction tmp(*this);
    --*this;
    return tmp;
}
Fraction::operator float() const {
    return (float)t / (float)m;
}
