#include <iostream>
#include <ctime>

class Date {
    int year, month, day;
    bool is_valid_date() const;
    Date(long);
    public:
    Date();
    Date(int, int=1, int=1);
    Date(const Date &);
    Date& operator = (const Date &);
    Date Tomorrow() const;
    Date Yesterday() const;

    bool operator == (const Date &) const;
    bool operator != (const Date &) const;
    bool operator <= (const Date &) const;
    bool operator >= (const Date &) const;
    bool operator < (const Date &) const;
    bool operator > (const Date &) const;

    Date operator + (int) const;
    Date operator - (int) const;
    Date& operator ++ ();
    Date& operator -- (); 
    Date operator ++ (int); 
    Date operator -- (int); 
    Date& operator += (int);
    Date& operator -= (int);

    friend std::ostream& operator << (std::ostream &, const Date &);
    friend std::istream& operator >> (std::istream &, Date &);

    operator int() const;
    operator long() const;
};

bool is_leap_year(int year) {
    return year%400==0 || (year%4==0 && year%100!=0);
}

const long month_sum[] = {0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365};

Date::Date(long l) {
    if (l < 0) {
        *this = Date(1,1,1);
        return;
    }
    year = l/365l + 1;
    long y = year - 1;
    l -= y*365l + y/4l - y/100l + y/400l;

    while (l < 0) {
        l += 365 + is_leap_year(y);
        y--, year--;
    }

    month = 1;
    while (l > month_sum[month] + (month>=2 && is_leap_year(year))) {
        month += 1;
    }
    day = l - (month_sum[month-1] + (month>2 && is_leap_year(year))) + 1;
}

Date::operator long() const {
    long r = 0;
    long y = year - 1;
    r += y*365l + y/4l - y/100l + y/400l;
    r += month_sum[month-1];
    if (month > 2 && is_leap_year(year)) r += 1;
    r += day;
    r -= 1;
    return r;
}

int month_day(int month, int year) {
    if (month==4 || month==6 || month==9 || month==11) return 30;
    if (month == 2) {
        if (is_leap_year(year)) return 29;
        return 28;
    }
    return 31;
}

bool Date::is_valid_date() const {
    if (year < 1) return false;
    if (month < 1 || month > 12) return false;
    if (day < 1 || day > month_day(month, year)) return false;
    return true;
}

Date::Date() {
    std::time_t t = std::time(nullptr);
    std::tm* tm = std::localtime(&t);
    year = tm->tm_year + 1900;
    month = tm->tm_mon + 1;
    day = tm->tm_mday;
}



Date::Date(int y, int m, int d): year(y), month(m), day(d) {
    if (!is_valid_date()) *this = Date(1,1,1);
}
Date::Date(const Date &d): year(d.year), month(d.month), day(d.day) {}
Date& Date::operator = (const Date &d) {
    year = d.year;
    month = d.month;
    day = d.day;
    return *this;
}

Date Date::operator + (int i) const {
    return Date((long)*this + (long)i);
}
Date Date::operator - (int i) const {
    return Date((long)*this - (long)i);
}
Date& Date::operator ++ () {
    *this += 1;
    return *this;
}
Date& Date::operator -- () {
    *this -= 1;
    return *this;
}
Date Date::operator ++ (int) {
    Date tmp(*this);
    *this += 1;
    return tmp;
}
Date Date::operator -- (int) {
    Date tmp(*this);
    *this -= 1;
    return tmp;
}
Date& Date::operator += (int i) {
    *this = *this + i;
    return *this;
}
Date& Date::operator -= (int i) {
    *this = *this - i;
    return *this;
}
Date Date::Tomorrow() const {
    return *this + 1;
}
Date Date::Yesterday() const {
    return *this - 1;
}

std::ostream& operator << (std::ostream& os, const Date &d) {
    return os << d.day << '/' << d.month << '/' << d.year << std::endl;
}
std::istream& operator >> (std::istream& is, Date &date) {
    int y, m, d;
    is >> y >> m >> d;
    date = Date(y, m, d);
    return is;
}

Date::operator int() const {
    return (int)((long)*this - (long)Date(year,1,1));
}

#define CMP_IMPL(op) bool Date::operator op (const Date &d) const {\
    return (long)*this op (long)d;\
}
CMP_IMPL(==)
CMP_IMPL(!=)
CMP_IMPL(<=)
CMP_IMPL(>=)
CMP_IMPL(<)
CMP_IMPL(>)
