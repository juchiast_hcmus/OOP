#include <cstdlib>
#include <cstdio>
#include <iostream>

// Array of intergers
class Array {
    size_t length;
    int *data;
    public:
    // Default constructor - empty array
    Array();
    
    // Clean up manually allocated memory
    ~Array();

    // Read and print array
    void read(FILE *in=stdin);
    void print(FILE *out=stdout) const;

    // Return number of elements of array 
    size_t size() const;

    // Get and set array's value
    int* get(size_t index);
    int& operator [](size_t index);
    void set(size_t index, int value);

    // Find first element with given value
    // Start searching from `pos`.
    int* find(int value, size_t pos=0);

    // Sort array - ascending order.
    void sort_asc();
    // Sort array - descending order.
    void sort_desc();
};

Array::Array(): length(0), data(NULL) {}

Array::~Array() {
    free(data);
}

void Array::read(FILE *in) {
    fscanf(in, "%zu", &length);
    if (data == NULL) {
        data = (int *) malloc(length * sizeof(int));
    } else {
        data = (int *) realloc(data, length * sizeof(int));
    }

    for (int *p = data, *end = data + length; p != end; p += 1) {
        fscanf(in, "%d", p);
    }
}

void Array::print(FILE *out) const {
    for (int *p = data, *end = data + length; p != end; p += 1) {
        fprintf(out, "%d ", *p);
    }
}

size_t Array::size() const {
    return length;
}

int* Array::get(size_t index) {
    if (index >= length) {
        return NULL;
    }
    return data + index;
}

int& Array::operator [] (size_t index) {
    return *(data + index);
}

void Array::set(size_t index, int value) {
    if (index >= length) {
        return;
    }
    *(data + index) = value;
}

int* Array::find(int value, size_t pos) {
    if (pos >= length) {
        return NULL;
    }

    for (int *p = data + pos, *end = data + length; p != end; p += 1) {
        if (*p == value) {
            return p;
        }
    }

    return NULL;
}

void sort(int *data, size_t length, bool asc) {
    for (size_t count = length; count > 0; count -= 1) {
        for (size_t i=1; i < length; i += 1) {
            if (asc && data[i-1] > data[i]) {
                int tmp = data[i-1];
                data[i-1] = data[i];
                data[i] = tmp;
            }
        }
    }
}

void Array::sort_asc() {
    if (length == 0) {
        return;
    }
    sort(data, length, true);
}

void Array::sort_desc() {
    if (length == 0) {
        return;
    }
    sort(data, length, false);
}
