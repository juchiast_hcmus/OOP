#include <iostream>

// Student's rank
enum class Rank {
    Gioi,
    Kha,
    TrungBinh,
    Yeu,
};

class Student {
    std::string name;
    float mathematic;
    float literature;
    public:
    Student();
    Student(const std::string &, float, float);

    // Get "Grade Point Average"
    float gpa() const;
    // Get rank
    Rank rank() const;

    // Set functions
    void set_name(const std::string &);
    void set_mathematic(float);
    void set_literature(float);

    // Get functions 
    std::string get_name() const;
    float get_mathematic() const;
    float get_literature() const;

    void read(std::istream &in=std::cin);
    void print(std::ostream &out=std::cout) const;
};

Student::Student(): name(), mathematic(0.0), literature(0.0) {}

Student::Student(const std::string &s, float m, float l):
    name(s),
    mathematic(m),
    literature(l) {}

float Student::gpa() const {
    return (mathematic + literature) / 2.0;
}

Rank Student::rank() const {
    float f = gpa();

    if (f >= 8.0) {
        return Rank::Gioi;
    }
    if (f >= 7.0) {
        return Rank::Kha;
    }
    if (f >= 5.0) {
        return Rank::TrungBinh;
    }
    return Rank::Yeu;
}

void Student::set_name(const std::string &s) {
    name = s;
}

void Student::set_mathematic(float f) {
    mathematic = f;
}

void Student::set_literature(float f) {
    literature = f;
}

std::string Student::get_name() const {
    return name;
}

float Student::get_literature() const {
    return literature;
}

float Student::get_mathematic() const {
    return mathematic;
}

void Student::read(std::istream &in) {
    std::getline(in, name);
    in >> mathematic;
    in >> literature;
}

void Student::print(std::ostream &out) const {
    using std::endl;
    out << "Ten      : " << name << endl;
    out << "Diem Toan: " << mathematic << endl;
    out << "Diem Van : " << literature << endl;
}
